package org.jastadd.exampleasteditor;

import java.io.FileNotFoundException;
import java.io.FileReader;

import org.jastadd.automaticasteditor.swing.CompilerContatiner;
import org.jastadd.automaticasteditor.swing.Main;
import org.jastadd.prettyprint.interfaces.Configuration;

import beaver.Parser;
import beaver.Scanner;
import gen.ExampleParser;
import gen.ExampleScanner;

public class EditorSwing {
	
	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration(ExampleScanner.class,ExampleParser.class);
		CompilerContatiner cc = new CompilerContatiner(conf, "/Users/alfred/git/jastadd-synthesizer/ExampleLang/testscript/example6.example", "/Users/alfred/git/jastadd-synthesizer/ExampleLang/gen/gen");
		
		
		new Main(cc);
	}

}
