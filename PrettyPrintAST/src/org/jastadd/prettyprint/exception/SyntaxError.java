package org.jastadd.prettyprint.exception;

public class SyntaxError extends RuntimeException {
	public SyntaxError(String msg) {
		super(msg);
	}
}
