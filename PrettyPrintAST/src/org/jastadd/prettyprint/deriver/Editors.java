package org.jastadd.prettyprint.deriver;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.management.RuntimeErrorException;

import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.interfaces.NodeListInterface;


public class Editors {

	public static void removeChild(final NodeInterface list, int index) {
		list.getChild(index).removeTokens();
		list.removeChild(index);
	}

	public static NodeInterface addToList(final NodeListInterface<NodeInterface> list, Class<?> toCreate,
			SubclassExtractor se) {
		NodeInterface node;
		try {
			node = (NodeInterface) toCreate.newInstance();
			se.inferAll(node);
			list.add(node);
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException("Error in getting List", e);
		}
		return node;
	}

	public static void editChild(NodeInterface node, String name, Class<?> itm, SubclassExtractor se) {
		try {

			NodeInterface o = (NodeInterface) itm.newInstance();
			se.inferAll(o);
			Method get = node.getClass().getMethod("get" + name);
			Method set = node.getClass().getMethod("set" + name, get.getReturnType());
			NodeInterface res = (NodeInterface) get.invoke(node);
			res.removeTokens();
			set.invoke(node, o);
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException("Error in getting List", e);
		}
	}

	public static void setValue(String token, String val, NodeInterface node) {
		try {
			node.getToken(token).setValue(val);
			Method mot = node.getClass().getMethod("set" + token, String.class);
			mot.invoke(node, val);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
			throw new RuntimeException("Can not find method: " + "set" + token, e);
		}
	}

	public static void removeOpt(NodeInterface node, String name) {
		try {
			Method get2 = node.getClass().getMethod("get" + name);
			Method get = node.getClass().getMethod("get" + name + "Opt");
			Method set = node.getClass().getMethod("set" + name + "Opt", get.getReturnType());
			Class<?> tmp0 = get.getReturnType();
			NodeInterface n = (NodeInterface) get2.invoke(node);
			n.removeTokens();
			set.invoke(node, tmp0.newInstance());
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | InstantiationException e) {
			throw new RuntimeException("Error in seting value", e);
		}
	}

	public static void addOpt(NodeInterface node, String name, Class<?> toCreate, SubclassExtractor se) {
		try {
			Method get = node.getClass().getMethod("get" + name + "Opt");
			Method set = node.getClass().getMethod("set" + name + "Opt", get.getReturnType());
			Class<?> tmp0 = get.getReturnType();
			Constructor<?>[] tmp = tmp0.getConstructors();
			Constructor<?> tmp1 = null;
			for (Constructor<?> t : tmp) {
				if (t.getParameterTypes().length == 1) {
					tmp1 = t;
				}
			}
			NodeInterface arg1 = (NodeInterface) toCreate.newInstance();
			se.inferAll(arg1);

			Object tmp2 = tmp1.newInstance(arg1);
			set.invoke(node, tmp2);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | InstantiationException e) {
			throw new RuntimeException("Error in seting value", e);
		}
	}


	public static boolean hasOpt(NodeInterface node, String optName) {
		boolean isSet = false;
		try {
			Method m1 = node.getClass().getMethod("has" + optName);
			 isSet = (boolean) m1.invoke(node);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new RuntimeException("Error in getting List", e);
		}
		return isSet;
	}
	
	public static Class<?>[] getClasses(NodeInterface node, String name,SubclassExtractor sce) {
		Class<?>[] classes = null;
		try {
			Method m = node.getClass().getMethod("get" + name);
			classes = sce.getSubClasses(m.getReturnType());
			
		} catch (NoSuchMethodException | SecurityException | IllegalArgumentException e) {
			throw new RuntimeException("Error in getting List", e);
		}
		return classes;
	}
	
	public static NodeListInterface<NodeInterface> extractItems(NodeInterface node, String listName) {
		final NodeListInterface<NodeInterface> list;
		try {
			Method m = node.getClass().getMethod("get" + listName + "List");
			list = (NodeListInterface<NodeInterface>) m.invoke(node);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new RuntimeException("Error in getting List", e);
		}
		return list;
	}
	
	public static NodeInterface extractItem(NodeInterface node, String list) {
		final NodeInterface res;
		try {
			Method m = node.getClass().getMethod("get" + list);
			res = (NodeInterface) m.invoke(node);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new RuntimeException("Error in getting List", e);
		}
		return res;
	}

	public static Class<?>[] getClassesList(NodeInterface node, String name, SubclassExtractor sce) {
		Class<?>[] classes = null;
		try {
			Method m = node.getClass().getMethod("get" + name,int.class);
			classes = sce.getSubClasses(m.getReturnType());
			
		} catch (NoSuchMethodException | SecurityException | IllegalArgumentException e) {
			throw new RuntimeException("Error in getting List: get" + name, e);
		}
		return classes;
	}
	
	public static String getValue(NodeInterface node,String token) {
		try {
			Method mot = node.getClass().getMethod("get" + token);
			return (String) mot.invoke(node);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
			throw new RuntimeException("Can not find method: " + "get" + token, e);
		}
	}
	
	public static Class<?> getCurrentClass(NodeInterface node,String name){
		try {
			Method m = node.getClass().getMethod("get" + name);
			Object o = m.invoke(node);
			return o.getClass();
		} catch (NoSuchMethodException | SecurityException | IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException("Error in getting List", e);
		}	
	}

	public static String getListNameFor(NodeInterface node) {
		if(!(node.getParent() instanceof NodeListInterface<?>)){
			throw new RuntimeException("Element not in list in: getListNameFor");
		}
		NodeInterface parent = node.getParent();
		NodeInterface grandParent = parent.getParent();
		String[] lists = grandParent.getLists();
		String res = null;
		for (int i = 0; i < lists.length; i++) {
			String name = lists[i];
			if(parent == extractItems(grandParent, name)){
				res = name;
			}
		}
		if(res == null){
			throw new RuntimeException("Element can not be found: getListNameFor");
		}
		return res;
	}
	
	public static String getNameFor(NodeInterface node) {
		NodeInterface parent = node.getParent();
		String[] children = parent.getChildren();
		String res = null;
		for (int i = 0; i < children.length; i++) {
			String name = children[i];
			if(node == extractItem(parent, name)){
				res = name;
			}
		}
		if(res == null){
			throw new RuntimeException("Element can not be found: getListNameFor");
		}
		return res;
	}
	
	public static int getListIndex(NodeInterface node ) {
		NodeInterface list = node.getParent();
		int res = -1;
		for(int i = 0; i < list.getNumChild(); i++){
			if(list.getChild(i) == node){
				res = i;
			}
		}	
		return res;
	}


	public static void changeElement(NodeInterface oldNode, Class<?> toCreate , SubclassExtractor se) {
		NodeInterface parent = oldNode.getParent();
		int index = getListIndex(oldNode);
		oldNode.removeTokens();
		NodeInterface node;
		try {
			node = (NodeInterface) toCreate.newInstance();
			se.inferAll(node);
			parent.setChild(node, index);
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException("Error in inserting value", e);
		}
		
	}
}
