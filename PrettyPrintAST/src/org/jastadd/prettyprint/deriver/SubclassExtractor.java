package org.jastadd.prettyprint.deriver;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.tokenlist.SignificantToken;

import beaver.Scanner.Exception;


public class SubclassExtractor {
	private int count = 0;

	private File path;

	private Configuration conf;

	public SubclassExtractor(File path,Configuration conf) {
		this.path = path;
		this.conf = conf;
	}

	public Class<?>[] getSubClasses(Class<?> o) {
		File[] files = path.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.getName().endsWith(".java");
			}
		});
		ArrayList<Class<?>> res = new ArrayList<Class<?>>();
		String pack = o.getPackage().getName();
		for (File f : files) {
			try {
				Object tmp = Class.forName(pack + "." + f.getName().substring(0, f.getName().length() - 5))
						.newInstance();
				if (o.isInstance(tmp)) {
					res.add(tmp.getClass());
				}
			} catch (InstantiationException e) {
			} catch (IllegalAccessException e) {
			} catch (ClassNotFoundException e) {
			}
		}
		return res.toArray(new Class<?>[0]);
	}

	public void inferAll(NodeInterface node) {
		for(int i = 1; i <10; i++){
			if(inferAll(node, i)){
				return;
			}
			
		}
		throw new RuntimeException("To complex abstract grammar can not infer all members");

	}
	
//	public boolean inferRandom(NodeInterface node, int level , long seed){
//		Random rand = new Random(seed);
//		if (level == 0) {
//			return false;
//		}
//		for (String childName : node.getChildren()) {
//			if (!canCreateChild(childName, node, level)) {
//				return false;
//			}
//		}
//		for(String opt : node.getOps()){
//			if(rand.nextFloat() < 0.5){
//				if(!canCreateOpt(opt,node,level)){
//					
//				}
//				
//			}
//			
//		}
//		fillTokens(node);
//		return false;
//		
//	}

	public boolean inferAll(NodeInterface node, int level) {
		if (level == 0) {
			return false;
		}
		for (String childName : node.getChildren()) {
			if (!canCreateChild(childName, node, level)) {
				return false;
			}
		}
		fillTokens(node);

		return true;

	}

	private boolean canCreateChild(String childName, NodeInterface node, int level) {
		Class<?>[] typesOfChildren = getTypesOfChildren(childName, node);

		ArrayList<NodeInterface> childCandidates = getChildCandidates(typesOfChildren);

		for (NodeInterface child : childCandidates) {
			if (child.getChildren().length == 0) {
				fillTokens(child);
				setValue(node, childName, child);
				return true;

			} else {
				boolean res = inferAll(child, level - 1);
				if (res) {
					fillTokens(child);
					setValue(node, childName, child);
					return true;
				}

			}

		}
		return false;
	}

	private ArrayList<NodeInterface> getChildCandidates(Class<?>[] typesOfChildren) {
		ArrayList<NodeInterface> childCandidates = new ArrayList<NodeInterface>();
		for (Class<?> c : typesOfChildren) {
			try {
				childCandidates.add((NodeInterface) c.newInstance());
			} catch (InstantiationException e) {
				throw new RuntimeException("Can not create instance", e);
			} catch (IllegalAccessException e) {
				throw new RuntimeException("Can not create instance", e);
			}
		}
		childCandidates.sort(new Comparator<NodeInterface>() {
			@Override
			public int compare(NodeInterface o1, NodeInterface o2) {
				int child = Integer.compare(o1.getChildren().length, o2.getChildren().length);
				if (child != 0) {
					return child;
				}
				int token = Integer.compare(o1.getTokens().length, o2.getTokens().length);
				if (token != 0) {
					return token;
				}
				int list = Integer.compare(o1.getLists().length, o2.getLists().length);
				if (list != 0) {
					return list;
				}
				return Integer.compare(o1.getOps().length, o2.getOps().length);
			}

		});
		return childCandidates;
	}

	private void setValue(NodeInterface node, String childName, NodeInterface child) {
		Method m;
		try {
			m = node.getClass().getMethod("set" + childName, getReturnType(childName, node));
			m.invoke(node, child); // Maybe have a default value
		} catch (NoSuchMethodException e) {
			throw new RuntimeException("Can not call method: set" + childName, e);
		} catch (SecurityException e) {
			throw new RuntimeException("Can not call method: set" + childName, e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException("Can not call method: set" + childName, e);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException("Can not call method: set" + childName, e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException("Can not call method: set" + childName, e);
		}
	}

	private void fillTokens(NodeInterface child) {
		for (String token : child.getTokens()) {
			try {
				Method m = child.getClass().getMethod("set" + token, String.class);
				m.invoke(child, child.getDefaultValue(token));
				SignificantToken st = new SignificantToken(child.getDefaultValue(token),conf.newScanner(new StringReader(child.getDefaultValue(token))).nextToken().getId());
			//	child.addToken(token, st );
			} catch (NoSuchMethodException e) {
				throw new RuntimeException("Can not call method: set" + token, e);
			} catch (SecurityException e) {
				throw new RuntimeException("Can not call method: set" + token, e);
			} catch (IllegalAccessException e) {
				throw new RuntimeException("Can not call method: set" + token, e);
			} catch (IllegalArgumentException e) {
				throw new RuntimeException("Can not call method: set" + token, e);
			} catch (InvocationTargetException e) {
				throw new RuntimeException("Can not call method: set" + token, e);
			} catch (IOException e) {
				throw new RuntimeException("Can not call method: set: IO problem" + token, e);
			} catch (Exception e) {
				throw new RuntimeException("Can not call method: set: Other problem" + token, e);
			}

		}

	}

	private Class<?>[] getTypesOfChildren(String childName, NodeInterface node) {
			return getSubClasses(getReturnType(childName, node));
	}

	private Class<?> getReturnType(String childName, NodeInterface node) {
		try {
			Method m = node.getClass().getMethod("get" + childName);
			Class<?> type = m.getReturnType();
			return type;
		} catch (NoSuchMethodException e) {
			throw new RuntimeException("Can not find mentod", e);
		} catch (SecurityException e) {
			throw new RuntimeException("Can not find mentod", e);
		}
	}

}
