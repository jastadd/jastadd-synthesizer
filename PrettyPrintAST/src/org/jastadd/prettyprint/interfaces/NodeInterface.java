package org.jastadd.prettyprint.interfaces;

import java.util.List;

import org.jastadd.prettyprint.formatter.TokenFormatter;
import org.jastadd.prettyprint.tokenlist.SignificantToken;
import org.jastadd.prettyprint.tokenlist.Token;



public interface NodeInterface {

	String[] getChildren();

	String[] getTokens();

	String[] getLists();

	String[] getOps();

	Token getStartToken();

	Token getEndToken();

	void setStartTokenRecursive(Token t);

	void setEndTokenRecursive(Token t);

	void setStartToken(Token t);

	void setEndToken(Token t);
	
	void changeAll(Token t);

	void setASTNodeToTokens();

	List<TokenFormatter> getFormat();

	void removeTokens();

	NodeInterface getChild(int i);

	void removeChild(int i);

	SignificantToken getToken(String name);

	void addToken(String name, SignificantToken token);

	boolean isChildOrParentOrSame(Object o);

	boolean isChild(Object o);

	String getDefaultValue(String token);

	boolean ownMatcher(String attr);

	boolean match(String attr, String s);

	List<NodeInterface> getPartents();
	
	NodeInterface getParent();

	Object getPart(Object o);
	
	Object getInlinePart(Object o);

	void flushTreeCache();
	
	void insertChild(NodeInterface node, int i);
	
	int getNumChild();
	
	void setChild(NodeInterface node, int i);
}
