package org.jastadd.prettyprint.interfaces;


public interface NodeListInterface<T extends NodeInterface> extends NodeInterface,Iterable<T> {
	NodeListInterface<T> add(T node);
}
