package org.jastadd.prettyprint.interfaces;

public interface NodeOptInterface<T extends NodeInterface> extends NodeInterface {
	
	T getValue();
	

}
