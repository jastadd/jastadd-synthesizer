package org.jastadd.prettyprint.interfaces;

import java.io.Reader;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import beaver.Parser;
import beaver.Scanner;

public class Configuration {
	
	public Class<? extends beaver.Scanner> scanner = null;
	public Class<? extends Parser> parser = null;
	
	public Scanner newScanner(Reader in){
		try {
			Constructor<? extends beaver.Scanner> consrutor = scanner.getConstructor(Reader.class);
			return consrutor.newInstance(in);
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException("Can not crete scanner", e);
		}
		
	}
	
	public Parser newParser(){
		try {
			return parser.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException("Can not create parser", e);
		}
		
	}

	public Configuration(Class<? extends Scanner> scanner, Class<? extends Parser> parser) {
		super();
		this.scanner = scanner;
		this.parser = parser;
	}

}
