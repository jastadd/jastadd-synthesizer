package org.jastadd.prettyprint.tokenlist;


public abstract class MiddleToken extends Token {
	private Token next = new EndToken();
	private Token prev = new StartToken();
	private String value;
	private boolean removed = false;

	@Override
	protected Token setNext(Token next) {
		testRemoved();
		Token res = this.next;
		this.next = next;
		return res;
	}

	@Override
	public Token getNext() {
		testRemoved();
		return next;
	}

	@Override
	public Token getPrev() {
		testRemoved();
		return prev;
	}

	@Override
	protected Token setPrev(Token prev) {
		testRemoved();
		Token res = this.prev;
		this.prev = prev;
		return res;
	}

	public MiddleToken(String value) {
		this.value = value;
		this.next = new EndToken();
		this.prev = new StartToken();
		next.setPrev(this);
		prev.setNext(this);
	}

	@Override
	public boolean hasValue() {
		testRemoved();
		return true;
	}

	@Override
	public String getValue() {
		testRemoved();
		return value;
	}

	@Override
	public boolean isStart() {
		testRemoved();
		return false;
	}

	@Override
	public boolean isEnd() {
		testRemoved();
		return false;
	}

	@Override
	public void remove() {
		
		if(getASTNode() == null){
			throw new RuntimeException("ASTNode is null for value: '" + value + "'\n prev is '" + getPrev().getValue() + "' next is '" + getNext().getValue() + "'" );
		}
		
		getASTNode().changeAll(this);
		
		
//		if(getASTNode() != null && getASTNode().getStartToken() == this){
//			getASTNode().setStartTokenRecursive(getNext());
//			
//		}
//		if(getASTNode() != null && getASTNode().getEndToken() == this){
//			getASTNode().setEndTokenRecursive(getPrev());
//		}
		
		doRemove();
		
	}
	/**
	 * Do not use, unless in testing
	 */
	public void doRemove(){
		
		this.getPrev().setNext(getNext());
		this.getNext().setPrev(getPrev());
		removed = true;
	}

	public void setValue(String value) {
		testRemoved();
		this.value = value;
	}
	
	private void testRemoved(){
		if (removed){
			System.out.println("This token is removed, and shall not be accessed: " + this);
			throw new RuntimeException("This token is removed, and shall not be accessed");
		}
	}

	@Override
	public String toString() {
		return "MiddleToken value="  + value + " : "+ super.toString();
	}

}
