package org.jastadd.prettyprint.tokenlist;

import java.awt.Component;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Iterator;

import org.jastadd.prettyprint.interfaces.NodeInterface;


public abstract class Token {

	public static Token createListStart() {
		StartToken start = new StartToken();
		EndToken end = new EndToken();
		start.setNext(end);
		end.setPrev(start);
		return start;
	}

	public static Token getStart(Token elem) {
		while (!elem.isStart()) {
			elem = elem.getPrev();
		}
		return elem;
	}

	public static Token getEnd(Token elem) {
		while (!elem.isEnd()) {
			elem = elem.getNext();
		}
		return elem;
	}

	public static void printInterval(Token elem1, Token elem2) {
		PrintStream ps = System.out;
		printInterval(elem1, elem2, ps);
	}

	private static void printInterval(Token elem1, Token elem2, PrintStream ps) {
		if (elem1 != null && elem2 != null) {
			Iterator<Token> itr = getIterator(elem1, elem2);
			while (itr.hasNext()) {
				ps.print(itr.next().getValue());
			}
		}
	}

	public static void setASTNodeOnTokens(Token elem1, Token elem2, NodeInterface o) {
		if (elem1 != null && elem2 != null) {
			Iterator<Token> itr = getIterator(elem1, elem2);
			while (itr.hasNext()) {
				itr.next().setNodeInterface(o);
			}
		}
	}

	/**
	 * Call allways hasNext before next.
	 * 
	 * @param elem1
	 * @param elem2
	 * @return
	 */
	public static Iterator<Token> getIterator(final Token elem1, final Token elem2) {
		return new Iterator<Token>() {
			private Token elem = elem1;

			@Override
			public boolean hasNext() {
				if(elem == null){
					return false;
				}
				if (elem == elem1 && elem1 instanceof StartToken) {
					elem = elem.getNext();
				}
				if (elem.isEnd()) {
					return false;
				}
				if (!elem2.isEnd()) {
					return elem != elem2.getNext();
				}
				return true;
			}

			@Override
			public Token next() {
				Token res = elem;
				elem = elem.getNext();
				return res;
			}
		};
	}

	public static Token createListEnd() {
		StartToken start = new StartToken();
		EndToken end = new EndToken();
		start.setNext(end);
		end.setPrev(start);
		return end;
	}

	private NodeInterface node;

	public void insertAfter(Token elem) {
		Token oldNext = setNext(elem);
		elem.setNext(oldNext);
		elem.setPrev(this);
		oldNext.setPrev(elem);
	}

	public void insertBefore(Token elem) {
		Token oldPrev = setPrev(elem);
		elem.setPrev(oldPrev);
		oldPrev.setNext(elem);
		elem.setNext(this);
	}

	public void printThisList() {
		printInterval(getStart(this), getEnd(this));
	}

	public String generate() {
		ByteArrayOutputStream bais = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(bais);
		printInterval(getStart(this), getEnd(this), ps);
		return new String(bais.toByteArray());

	}

	public Token getNodeAtIndex(int index) {
		Iterator<Token> itr = getIterator(getStart(this), getEnd(this));
		int currentLengt = 0;
		Token currentToken = null;
		while (itr.hasNext()) {
			currentToken = itr.next();
			currentLengt += currentToken.getValue().length();
			if (index < currentLengt) {
				return currentToken;
			}
		}
		return currentToken;

	}
	
	public int getEndIndexAtToken(){
		Iterator<Token> itr = getIterator(getStart(this), this);
		int currentLengt = 0;
		Token currentToken = null;
		while (itr.hasNext()) {
			currentToken = itr.next();
			currentLengt += currentToken.getValue().length();
		}
		return currentLengt;
	}

	public abstract void remove();

	public abstract Token getNext();

	public abstract Token getPrev();

	public abstract boolean isStart();

	public abstract boolean isEnd();

	protected abstract Token setNext(Token next);

	protected abstract Token setPrev(Token prev);

	public abstract boolean hasValue();

	public abstract String getValue();

	public NodeInterface getASTNode() {
		return node;
	}

	public void setNodeInterface(NodeInterface o) {
		node = o;

	}
	
	public boolean isBefore(Token t){
		Token current = this;
		while(current != t && !current.isEnd()){
			current = current.getNext();
		}
		return current == t;
	}
	
	public boolean isAfter(Token t){
		Token current = this;
		while(current != t && !current.isStart()){
			current = current.getPrev();
		}
		return current == t;
	}
	
	public Token getStartToken(){
		return Token.getStart(this);
	}
	
	public Token getEndToken(){
		return Token.getEnd(this);
	}

	public Object getInlinePart(Object o) {
		return null;
	}
	
	public void setInlinePartCreator(Object o) {
	}

}
