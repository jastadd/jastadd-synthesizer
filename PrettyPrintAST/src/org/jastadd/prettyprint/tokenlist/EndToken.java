package org.jastadd.prettyprint.tokenlist;

public class EndToken extends Token{
	
	private Token prev;
	
	@Override
	protected Token setNext(Token next) {
		throw new RuntimeException("Try to set next on a EndTokenListElement");
	}

	@Override
	public Token getNext() {
		throw new RuntimeException("Can not return next of EndTokenListElement because it do not exists.");
	}

	@Override
	public Token getPrev() {
		return prev;
	}


	@Override
	protected Token setPrev(Token prev) {
		Token res = this.prev;
		this.prev = prev;
		return res;
	}
	
	protected EndToken() {
	}

	@Override
	public boolean hasValue() {
		return false;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public boolean isStart() {
		return false;
	}

	@Override
	public boolean isEnd() {
		return true;
	}

	@Override
	public void remove() {
		throw new RuntimeException("Can not remove EndTokenListElement");
	}
}
