package org.jastadd.prettyprint.tokenlist;

public class SignificantToken extends MiddleToken {

	private short token;
	private String name;

	public SignificantToken(String value, short token) {
		super(value);
		this.token = token;
	}

	public short getToken() {
		return token;
	}
	
	public void setName(String name){
		this.name = name;
		
		
	}
	
	public String getName(){
		return name;
	}
	
	public boolean hasName(){
		return name != null;
	}
	
	@Override
	public String toString() {
		return super.toString() + " => " + getValue();
	}
	
	

}