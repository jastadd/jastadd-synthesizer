package org.jastadd.prettyprint.tokenlist;

public class StartToken extends Token {
	private Token next;
	
	@Override
	protected Token setNext(Token next) {
		Token res = this.next;
		this.next = next;
		return res;
	}

	@Override
	public Token getNext() {
		return next;
	}

	@Override
	public Token getPrev() {
		throw new RuntimeException("Can not return prev of StartTokenListElement because it do not exists.");
	}

	@Override
	protected Token setPrev(Token prev) {
		throw new RuntimeException("Try to set prev on a StartTokenListElement");
	}

	@Override
	public boolean hasValue() {
		return false;
	}

	@Override
	public String getValue() {
		return null;
	}

	@Override
	public boolean isStart() {
		return true;
	}

	@Override
	public boolean isEnd() {
		return false;
	}

	@Override
	public void remove() {
		throw new RuntimeException("Can not remove StartTokenListElement");
	}

}
