package org.jastadd.prettyprint.formatter;

import java.io.IOException;
import java.io.StringReader;

import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.tokenlist.CommentToken;
import org.jastadd.prettyprint.tokenlist.SignificantToken;
import org.jastadd.prettyprint.tokenlist.Token;
import org.jastadd.prettyprint.tokenlist.WhiteSpaceToken;

import beaver.Scanner.Exception;

public class AcceptToken implements TokenFormatter {

	private String tokenString;
	private short id = -1;
	private String tokenName;
	private NodeInterface node;

	public AcceptToken(String tokenString, NodeInterface node) {
		this.tokenString = tokenString;
		this.node = node;
	}
	
	public void setConf(Configuration conf){
		beaver.Scanner scanner = conf.newScanner(new StringReader(tokenString));
		try {
			id = scanner.nextToken().getId();
		} catch (IOException e) {
			throw new RuntimeException("Scanner IO problem", e);
		} catch (Exception e) {
			throw new RuntimeException("Scanner problem", e);
		}
	}

	public AcceptToken(String tokenString, String tokenName, NodeInterface node) {
		this(tokenString,node);
		this.tokenName = tokenName;
	}

	@Override
	public FormatResult format(Token startElem, int indentations) {
		Token prevToken = startElem;
		while (!prevToken.getNext().isEnd() && prevToken.getNext() instanceof WhiteSpaceToken) {
			prevToken.getNext().remove();
		}
		if (prevToken.getNext() instanceof CommentToken) {
			Token first = prevToken.getNext();
			prevToken = prevToken.getNext();
			prevToken = (new NewLineToken()).format(prevToken, indentations).getLast();
			FormatResult tmp = format(prevToken, indentations);
			return new FormatResult(first, tmp.getLast(), indentations);
		} else {
			if (prevToken.getNext() instanceof SignificantToken) {
				SignificantToken token = (SignificantToken) prevToken.getNext();
				if (token.getValue().equals(tokenString) && token.getASTNode() == node) {
					
				}
				else {
					SignificantToken newToken = new SignificantToken(tokenString, id);
					prevToken.insertAfter(newToken);
					if (tokenName != null) {
						node.addToken(tokenName, newToken);
					}
				}
			} else {
				prevToken.insertAfter(new SignificantToken(tokenString, id));
			}
			return new FormatResult(prevToken.getNext(), prevToken.getNext(), indentations);
		}

	}

	@Override
	public String toString() {
		return "AcceptToken [tokenString=" + tokenString + ", Node=" + node + ", id=" + id + "]";
	}

}
