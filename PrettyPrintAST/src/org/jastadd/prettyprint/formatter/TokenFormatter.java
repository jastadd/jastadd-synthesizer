package org.jastadd.prettyprint.formatter;

import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.tokenlist.Token;


public interface TokenFormatter {
	/**
	 * 
	 * @param startElem Is the element before the first
	 * @param indentations
	 * @return 
	 */
	public FormatResult format(Token startElem, int indentations);
	public void setConf(Configuration conf);

}
