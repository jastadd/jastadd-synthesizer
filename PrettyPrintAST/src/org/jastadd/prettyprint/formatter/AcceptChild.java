package org.jastadd.prettyprint.formatter;

import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.tokenlist.Token;


public class AcceptChild implements TokenFormatter {
	private NodeInterface node;
	private Configuration conf;

	public AcceptChild(NodeInterface node){
		this.node = node;
	}

	@Override
	public FormatResult format(Token startElem, int indentations) {
		return PrettyMaker.makePretty(this.node, indentations, startElem,conf);
	}

	@Override
	public String toString() {
		return "AcceptChild [node=" + node + "]";
	}

	@Override
	public void setConf(Configuration conf) {
		this.conf = conf;
		
	}
}
