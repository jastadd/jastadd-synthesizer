package org.jastadd.prettyprint.formatter;

import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.tokenlist.CommentToken;
import org.jastadd.prettyprint.tokenlist.SignificantToken;
import org.jastadd.prettyprint.tokenlist.Token;
import org.jastadd.prettyprint.tokenlist.WhiteSpaceToken;

public class FixEnd implements TokenFormatter {
	
	private NodeInterface node;

	public FixEnd(NodeInterface node) {
		this.node = node;
	}

	@Override
	public FormatResult format(Token startElem, int indentations) {
		Token prevToken = startElem;
		if(prevToken.isEnd()){
			return new FormatResult(prevToken, prevToken, indentations);
		}
		// System.out.println(prevToken.generate());
		while (!prevToken.getNext().isEnd() && prevToken.getNext() instanceof WhiteSpaceToken && node.isChild(prevToken.getNext().getASTNode())) {
			prevToken.getNext().remove();
		}
		if (prevToken.getNext() instanceof CommentToken && node.isChild(prevToken.getNext().getASTNode())) {
			Token first = prevToken.getNext();
			prevToken = prevToken.getNext();
			prevToken = (new NewLineToken()).format(prevToken, indentations).getLast();
			FormatResult tmp = format(prevToken, indentations);
			return new FormatResult(first, tmp.getLast(), indentations);
		} else {
			if (prevToken.getNext() instanceof SignificantToken && node.isChild(prevToken.getNext().getASTNode())) {
				prevToken.getNext().remove();
				return format(prevToken, indentations);
			}
		}
		return new FormatResult(prevToken, prevToken, indentations);
	}

	@Override
	public void setConf(Configuration conf) {
	}

}
