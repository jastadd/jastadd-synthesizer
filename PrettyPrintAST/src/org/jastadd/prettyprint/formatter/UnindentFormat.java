package org.jastadd.prettyprint.formatter;

import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.tokenlist.Token;


public class UnindentFormat implements TokenFormatter {

	@Override
	public FormatResult format(Token startElem, int indentations) {
		Token prevToken = startElem;
		int currendIndetation = indentations - 1;
		return new FormatResult(prevToken, prevToken, currendIndetation);
	}

	@Override
	public void setConf(Configuration conf) {
		
	}

}
