package org.jastadd.prettyprint.formatter;

import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.tokenlist.CommentToken;
import org.jastadd.prettyprint.tokenlist.Token;
import org.jastadd.prettyprint.tokenlist.WhiteSpaceToken;


public class NewLineToken implements TokenFormatter {

	@Override
	public FormatResult format(Token startElem, int indentations) {
		Token prevToken = startElem;
		if(prevToken.isEnd()){
			prevToken.insertBefore(new WhiteSpaceToken("\n"));
			Token t = indent(indentations, prevToken.getPrev());
			return new FormatResult(prevToken.getPrev(), t, indentations);
		}
		if (prevToken.getNext() instanceof WhiteSpaceToken) {
			WhiteSpaceToken token = (WhiteSpaceToken) prevToken.getNext();
			if (token.getValue().equals("\n")) {
				Token t = indent(indentations, prevToken.getNext());
				return new FormatResult(prevToken.getNext(), t, indentations);
			} else {
				prevToken.getNext().remove();
				return format(prevToken, indentations);
			}
		} else if (prevToken.getNext() instanceof CommentToken) {
			return format(prevToken.getNext(), indentations);
		} else {
			prevToken.insertAfter(new WhiteSpaceToken("\n"));
			Token t = indent(indentations, prevToken.getNext());
			return new FormatResult(prevToken.getNext(), t, indentations);
		}
	}

	private Token indent(int indet, Token prev) {
		if (indet == 0) {
			return prev;
		} else {
			if (prev.getNext() instanceof WhiteSpaceToken) {
				WhiteSpaceToken token = (WhiteSpaceToken) prev.getNext();
				if (token.getValue().equals("\t")) {
					return indent(indet - 1, token);
				} else {
					prev.insertAfter(new WhiteSpaceToken("\t"));
					return indent(indet - 1, prev.getNext());
				}
			} else {
				prev.insertAfter(new WhiteSpaceToken("\t"));
				return indent(indet - 1, prev.getNext());
			}

		}

	}

	@Override
	public void setConf(Configuration conf) {
		
	}

}
