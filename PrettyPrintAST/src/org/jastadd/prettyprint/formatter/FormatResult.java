package org.jastadd.prettyprint.formatter;

import org.jastadd.prettyprint.tokenlist.Token;

public final class FormatResult {
	
	private final Token first;
	private final Token last;
	private final int indent;
	
	/**
	 * @param first inclusive
	 * @param last inclusive
	 */
	public FormatResult(Token first, Token last, int indent) {
		super();
		this.first = first;
		this.last = last;
		this.indent = indent;
	}

	public Token getFirst() {
		return first;
	}

	public Token getLast() {
		return last;
	}
	
	public int getIndent(){
		return indent;
	}

}
