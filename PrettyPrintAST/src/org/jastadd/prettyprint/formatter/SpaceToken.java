package org.jastadd.prettyprint.formatter;

import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.tokenlist.Token;
import org.jastadd.prettyprint.tokenlist.WhiteSpaceToken;


public class SpaceToken implements TokenFormatter {

	@Override
	public FormatResult format(Token startElem,  int indentations) {
		Token prevToken = startElem;
		if (prevToken.getNext() instanceof WhiteSpaceToken) {
			WhiteSpaceToken token = (WhiteSpaceToken) prevToken.getNext();
			if (token.getValue().equals(" ")) {
				;
			} else {
				prevToken.insertAfter(new WhiteSpaceToken(" "));
			}
		} else {
			prevToken.insertAfter(new WhiteSpaceToken(" "));
		}
		return new FormatResult(prevToken.getNext(),  prevToken.getNext(), indentations);
	}

	@Override
	public void setConf(Configuration conf) {
	
	}

}
