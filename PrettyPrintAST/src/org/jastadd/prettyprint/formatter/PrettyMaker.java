package org.jastadd.prettyprint.formatter;

import java.util.Iterator;
import java.util.List;

import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.tokenlist.SignificantToken;
import org.jastadd.prettyprint.tokenlist.Token;


public class PrettyMaker {

	public static void makePretty(NodeInterface node,Configuration conf) {
		if(node.getStartToken() != null){
			node.setStartToken(Token.getStart(node.getStartToken()));
		}
		if(node.getEndToken() != null){
			node.setEndToken(Token.getEnd(node.getEndToken()));
		}
		
		node.setASTNodeToTokens();
		if(node.getStartToken() == null){
			makePretty(node,0,Token.createListStart(),conf);
			
		}else{
//			Token.getIterator(node.getStartToken(), node.getEndToken());
			makePretty(node, 0, Token.getStart(node.getStartToken()),conf);
		}
		node.setASTNodeToTokens();
		
//		System.out.println("First done!!!!!!!!");
//		
//		String last = null;
//		String current = node.getStartToken().generate();
		
//		do{
//			last = current;
//			node.setASTNodeToTokens();
//			makePretty(node, 0, Token.getStart(node.getStartToken()),conf);
//			current = node.getStartToken().generate();
//			System.out.println("Current:");
//			System.out.println(current);
//			System.out.println("Last:");
//			System.out.println(last);
//		}
//		while(!last.equals(current));
		System.out.println("Prettyprint over!");
		
		
		
		
		
//		node.setNodeInterfaceToTokens();
//		Iterator<Token> itr = Token.getIterator(node.getEndToken().getNext(), Token.getEnd(node.getEndToken()));
//		while(itr.hasNext()){
//			Token t = itr.next();
//			if(t instanceof SignificantToken){
//				t.remove();
//			}
//		}
	}

	public static FormatResult makePretty(NodeInterface node, int indent, Token start,Configuration conf) {
		List<TokenFormatter> formatterList = node.getFormat();
		
		formatterList.add(new FixEnd(node));
		for(TokenFormatter tf :formatterList){
			tf.setConf(conf);
		}

		if (formatterList.size() == 1) {
			throw new RuntimeException("When you see this, you have an empty formatlist for node: " + node.getClass().getName());
		}
		Token elem = start;
		int currentIndent = indent;
		FormatResult formatResult = formatterList.get(0).format(start, currentIndent);
		Token firstToken = formatResult.getFirst();
		elem = formatResult.getLast();
		currentIndent = formatResult.getIndent();
		for (int i = 1; i < formatterList.size(); i++) {
			formatResult = formatterList.get(i).format(elem, currentIndent);
			elem = formatResult.getLast();
			currentIndent = formatResult.getIndent();
		}
//		if(node.getStartToken() == null || firstToken.isBefore(node.getStartToken())){
			node.setStartTokenRecursive(firstToken);
//		}
	
//		if(node.getEndToken() == null || elem.isAfter(node.getEndToken())){
			node.setEndTokenRecursive(elem);
//		}
		return new FormatResult(firstToken, elem, currentIndent);
	}
}
