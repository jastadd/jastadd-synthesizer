package org.jastadd.prettyprint.formatter;

import java.util.Iterator;
import java.util.List;

import org.jastadd.prettyprint.interfaces.NodeInterface;

public class Helpers {
	
	public static void createBlock(List<TokenFormatter> res, String startToken, String endToken, Iterable<? extends NodeInterface> nes, NodeInterface node){
		res.add(new AcceptToken(startToken,node));
		res.add(new IndentFormat());
		res.add(new NewLineToken());
		boolean runed = false;
		for(NodeInterface ne : nes){
			res.add(new AcceptChild(ne));
			res.add(new NewLineToken());
			runed = true;
		}
		if(runed){
			res.remove(res.size()-1);
		}
		res.add(new UnindentFormat());
		res.add(new NewLineToken());
		res.add(new AcceptToken(endToken,node));
	}

}
