package org.jastadd.prettyprint.formatter;

import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.tokenlist.Token;
import org.jastadd.prettyprint.tokenlist.WhiteSpaceToken;


public class WhiteSpaceSpecToken implements TokenFormatter {
	private String space; 
	
	public WhiteSpaceSpecToken(String spece) {
		this.space = spece;
	}

	@Override
	public FormatResult format(Token startElem,  int indentations) {
		Token prevToken = startElem;
		if (prevToken.getNext() instanceof WhiteSpaceToken) {
			WhiteSpaceToken token = (WhiteSpaceToken) prevToken.getNext();
			if (token.getValue().equals(space)) {
				;
			} else {
				prevToken.insertAfter(new WhiteSpaceToken(space));
			}
		} else {
			prevToken.insertAfter(new WhiteSpaceToken(space));
		}
		return new FormatResult(prevToken.getNext(),  prevToken.getNext(), indentations);
	}

	@Override
	public void setConf(Configuration conf) {
		
	}

}
