package org.jastadd.automaticasteditor;

import org.jastadd.prettyprint.interfaces.NodeInterface;

import javafx.event.EventHandler;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class ParentListPane extends VBox implements EventHandler<MouseEvent> {

	private ListView parents;
	private RightSide rightPane;

	public ParentListPane(NodeInterface node,RightSide rightPane) {
		parents = new ListView<>();
		parents.getItems().addAll(node.getPartents());
		parents.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
		getChildren().add(parents);
		this.rightPane = rightPane;

	}

	@Override
	public void handle(MouseEvent event) {
		rightPane.changeNode((NodeInterface) parents.getSelectionModel().getSelectedItem());
	}

}
