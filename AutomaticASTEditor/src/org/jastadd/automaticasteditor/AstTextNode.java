package org.jastadd.automaticasteditor;

import javafx.scene.text.Text;

public class AstTextNode extends Text {
	private Object astNode;
	
	public AstTextNode(String text, Object astNode){
		super(text);
		this.astNode = astNode;
	}
}
