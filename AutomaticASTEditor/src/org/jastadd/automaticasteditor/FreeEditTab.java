package org.jastadd.automaticasteditor;

import java.io.IOException;
import java.io.StringReader;

import org.jastadd.prettyprint.exception.SyntaxError;
import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.interfaces.NodeInterface;

import beaver.Parser;
import beaver.Parser.Exception;
import beaver.Scanner;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;

public class FreeEditTab extends Tab {

	private TextArea ta;

	public FreeEditTab() {
		setText("Free edit");
		ta = new TextArea(MainWindow.rootNode.getStartToken().generate());
		setContent(ta);
	}

	public boolean canSwitch() {
		try {
			Scanner scanner = Configuration.newScanner(new StringReader(ta.getText()));
			Parser parser = Configuration.newParser();
			MainWindow.rootNode = (NodeInterface) parser.parse(scanner);
		} catch (Exception | IOException | SyntaxError e) {
			Alert a = new Alert(AlertType.INFORMATION);
			a.setTitle("Error");
			a.setContentText(e.getMessage());
//			a.showAndWait();
			a.show();
			return false;
		}
		return true;
	}

	public void hasBeenSelected() {
		ta.setText(MainWindow.rootNode.getStartToken().generate());
	}
}
