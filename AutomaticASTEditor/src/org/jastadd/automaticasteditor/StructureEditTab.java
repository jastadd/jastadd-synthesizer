package org.jastadd.automaticasteditor;


import org.jastadd.prettyprint.formatter.PrettyMaker;
import org.jastadd.prettyprint.tokenlist.CommentToken;
import org.jastadd.prettyprint.tokenlist.Token;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public class StructureEditTab extends Tab {
	private TextArea ta;
	private RightSide rightSide;

	public <T> StructureEditTab() {
		setText("Structure edit");
		BorderPane bp = new BorderPane();

		ta = new TextArea(MainWindow.rootNode.getStartToken().generate());
		ta.setCursor(Cursor.HAND);
		ta.setEditable(false);
		ta.addEventHandler(MouseEvent.MOUSE_CLICKED, new HandleClick());
		
		ta.selectRange(1, 12);
		
		bp.setCenter(ta);

		rightSide = new RightSide(ta, MainWindow.rootNode);
//		rightSide.setMinWidth(400);
//		rightSide.setMaxWidth(400);
//		rightSide.setPrefWidth(400);
//	//	vb.getChildren().add(new Label("hej"));
//		rightSide.getChildren().clear();
//		rightSide.getChildren().add(new NodeEditArea(MainWindow.rootNode,rightSide,ta));
		
		bp.setRight(rightSide);
		
		ta.setId("textIsShownHere");

		MainWindow.treeChanged.addListener(new ChangeListener<Boolean>() {

			@Override
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
//				PrettyMaker.makePretty(MainWindow.rootNode);
//				Scanner scanner = Configuration
//						.newScanner(new StringReader(MainWindow.rootNode.getStartToken().generate()));
//				Parser parser = Configuration.newParser();
//				try {
//					MainWindow.rootNode = (NodeInterface) parser.parse(scanner);
//				} catch (Exception | IOException | SyntaxError e) {
//					throw new RuntimeException("Can not parse what generated from editor: ", e);
//				}
				PrettyMaker.makePretty(MainWindow.rootNode);
				ta.setText(MainWindow.rootNode.getStartToken().generate());
//				vb.getChildren().clear();
//				vb.getChildren().add(new Label("hej"));
			}
		});

		setContent(bp);
	}

	public boolean canSwitch() {
		return true;
	}

	public void hasBeenSelected() {
		PrettyMaker.makePretty(MainWindow.rootNode);
		ta.setText(MainWindow.rootNode.getStartToken().generate());
	}

	class HandleClick implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			Token clickedToken = MainWindow.rootNode.getStartToken().getNodeAtIndex(ta.getCaretPosition());
			if (clickedToken.getASTNode() != null && !(clickedToken instanceof CommentToken)) {
//				NodeEditArea nea = new NodeEditArea(clickedToken.getASTNode(),rightSide,ta);
//				rightSide.getChildren().clear();
//				rightSide.getChildren().add(nea);
				rightSide.changeNode(clickedToken.getASTNode());
			}
		}

	}

}