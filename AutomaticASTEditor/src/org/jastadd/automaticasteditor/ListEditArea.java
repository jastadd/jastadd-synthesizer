package org.jastadd.automaticasteditor;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.deriver.SubclassExtractor;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.interfaces.NodeListInterface;

import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public class ListEditArea extends BorderPane {

	public ListEditArea(NodeInterface node, String listName) {
		ListView<NodeInterface> lw = new ListView<NodeInterface>();
		lw.setMinWidth(50);
		lw.setMaxWidth(350);
		ObservableList<NodeInterface> items = lw.getItems();
		lw.setPrefHeight(100);
		lw.autosize();
		final NodeListInterface<NodeInterface> list = extractItems(node, listName, items);
		VBox v = new VBox();
		v.getChildren().add(new Label(listName + "*"));
		v.getChildren().add(lw);
		
		setTop(v);
		addRemoveButton(lw, items, list);
		ChoiceBox<Class<?>> cb = new ChoiceBox<>();
		cb.setMinWidth(50);
		cb.setMaxWidth(350);
		getAllSubclasses(node, listName, cb);
		setCenter(cb);
		addAddButton(items, list, cb);
	}

	private void getAllSubclasses(NodeInterface node, String listName, ChoiceBox<Class<?>> cb) {
		ObservableList<Class<?>> cbItems = cb.getItems();
		try {
			Method m = node.getClass().getMethod("get" + listName, int.class);
			m.getReturnType();
			Class<?>[] classes = MainWindow.subClassExtractor.getSubClasses(m.getReturnType());
			cbItems.addAll(classes);
			cb.getSelectionModel().select(0);
		} catch (NoSuchMethodException | SecurityException | IllegalArgumentException e) {
			throw new RuntimeException("Error in getting List", e);
		}
	}

	private NodeListInterface<NodeInterface> extractItems(NodeInterface node, String listName,
			ObservableList<NodeInterface> items) {
		final NodeListInterface<NodeInterface> list;
		try {
			Method m = node.getClass().getMethod("get" + listName + "List");
			list = (NodeListInterface<NodeInterface>) m.invoke(node);
			for (NodeInterface a : list) {
				items.add(a);
			}
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new RuntimeException("Error in getting List", e);
		}
		return list;
	}

	private void addRemoveButton(final ListView<NodeInterface> lw,final ObservableList<NodeInterface> items,
			final NodeListInterface<NodeInterface> list) {
		Button btn = new Button("Remove");
		setBottom(btn);
		btn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				int index = lw.getSelectionModel().getSelectedIndex();
				items.remove(index);
				Editors.removeChild(list, index);
				MainWindow.treeChanged.setChanged();
			}
		});
	}

	private void addAddButton(final ObservableList<NodeInterface> items, final NodeListInterface<NodeInterface> list,
			final ChoiceBox<Class<?>> cb) {
		Button addBtn = new Button("Add");
		addBtn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				Class<?> toCreate = cb.getSelectionModel().getSelectedItem();
				NodeInterface node;
				node = Editors.addToList(list, toCreate, MainWindow.subClassExtractor);
				items.add(node);
				MainWindow.treeChanged.setChanged();
			}

		});
		setLeft(addBtn);
	}

}
