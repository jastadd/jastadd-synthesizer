package org.jastadd.automaticasteditor;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.deriver.SubclassExtractor;
import org.jastadd.prettyprint.interfaces.NodeInterface;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.BorderPane;

public class OpsEditArea extends BorderPane {

	public OpsEditArea(final NodeInterface node,final String name) {
		CheckBox cb = new CheckBox("[" + name + "]");
		setCenter(cb);

		final ChoiceBox<Class<?>> cb1 = new ChoiceBox<>();
		try {
			Method m1 = node.getClass().getMethod("has" + name);
			boolean isSet = (boolean) m1.invoke(node);
			cb.selectedProperty().set(isSet);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new RuntimeException("Error in getting List", e);
		}
		ObservableList<Class<?>> cbItems = cb1.getItems();
		try {
			Method m = node.getClass().getMethod("get" + name);
			Class<?>[] classes = MainWindow.subClassExtractor.getSubClasses(m.getReturnType());
			cbItems.addAll(classes);
			cb1.getSelectionModel().select(0);
			
		} catch (NoSuchMethodException | SecurityException | IllegalArgumentException e) {
			throw new RuntimeException("Error in getting List", e);
		}
		setRight(cb1);

		cb.selectedProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(final ObservableValue<? extends Boolean> observable,final Boolean oldValue,final Boolean newValue) {
				if (newValue) {
					Class<?> toCreate = cb1.getSelectionModel().getSelectedItem();
					Editors.addOpt(node, name, toCreate, MainWindow.subClassExtractor);
					MainWindow.treeChanged.setChanged();

				} else {
					Editors.removeOpt(node, name);
					MainWindow.treeChanged.setChanged();
				}

			}
		});
	}
}
