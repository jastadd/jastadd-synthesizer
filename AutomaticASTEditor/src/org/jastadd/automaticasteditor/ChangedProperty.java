package org.jastadd.automaticasteditor;

import javafx.beans.property.SimpleBooleanProperty;

public class ChangedProperty extends SimpleBooleanProperty {
	
	public ChangedProperty(){
		super(false);
	}
	
	public void setChanged(){
		this.setValue(true);
		this.setValue(false);
	}

}
