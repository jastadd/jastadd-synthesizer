package org.jastadd.automaticasteditor;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.interfaces.NodeInterface;

import beaver.Scanner;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SkinBase;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.paint.Color;

public class TextEditArea extends BorderPane implements EventHandler<MouseEvent> {

	private NodeInterface node;
	private TextField textField;
	private String token;

	public TextEditArea(NodeInterface node, String token) {
		this.node = node;
		this.token = token;
		setLeft(new Label("<" + token + ">"));
		this.textField = new TextField(getValue(token));
		textField.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
		setCenter(textField);
		Button change = new Button("Set");
		setRight(change);
		change.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
		
		
	}
	
	private String getValue(String token) {
		try {
			Method mot = node.getClass().getMethod("get" + token);
			return (String) mot.invoke(node);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			e.printStackTrace();
			throw new RuntimeException("Can not find method: " + "get" + token, e);
		}
	}

	@Override
	public void handle(MouseEvent event) {
		
		String line = textField.getText();
		Scanner es = Configuration.newScanner(new StringReader(line));
		try {
			boolean matchToken = (es.nextToken().getId() == ((NodeInterface) node).getToken(token).getToken()
					&& es.nextToken().getId() == 0);
			boolean match = node.ownMatcher(token) ? node.match(token, line) : matchToken;
			if (match) {
				Editors.setValue(token, line, node);
				MainWindow.treeChanged.setChanged();
				textField.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
			} else {
				textField.setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));

			}
		} catch (IOException | beaver.Scanner.Exception e1) {
			throw new RuntimeException("Io problem: " + "set" + token, e1);
		}
		
	}
}
