package org.jastadd.automaticasteditor;

import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.interfaces.NodeInterface;

import beaver.Scanner;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class NodeEditArea extends GridPane{ 

	private NodeInterface node;
	private String[] tokens;
	private String[] ops;
	private String[] lists;
	private String[] children;
	private List<TextField> textFields = new ArrayList<>();

	public <T> NodeEditArea(NodeInterface node, RightSide rightPane) {
		node.getStartToken();

		this.node = node;
		tokens = this.node.getTokens();
		ops = this.node.getOps();
		lists = this.node.getLists();
		children = this.node.getChildren();

		int count = 0;
		
		add(new Label(node.getClass().getSimpleName() + "::="),0,count);
		count ++;

		count = tokens.length + 1;
		for (int i = 0; i < children.length; i++) {
			add(new ChildEditArea((NodeInterface) node, children[i]), 1, count + i);
		}

		count += children.length + 1;
		for (int i = 0; i < ops.length; i++) {
			add(new OpsEditArea((NodeInterface) node, ops[i]), 1, count + i);
		}

		count += ops.length + 1;
		for (int i = 0; i < lists.length; i++) {
			add(new ListEditArea((NodeInterface) node, lists[i]), 1, count + i);
		}
		for (int i = 0; i < tokens.length; i++) {
			add(new TextEditArea(node, tokens[i]), 1, count + i);
		}
		count += lists.length;
		
		add(new ParentListPane(node, rightPane), 1, count + 1);

		
	}

}
