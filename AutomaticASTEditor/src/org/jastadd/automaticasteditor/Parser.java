package org.jastadd.automaticasteditor;

import java.io.IOException;
import java.io.StringReader;

import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.interfaces.NodeInterface;

import beaver.Parser.Exception;
import beaver.Scanner;

public class Parser {
	
	public static void reParse() throws ParserExeption {
		Scanner scanner = Configuration.newScanner(new StringReader(MainWindow.rootNode.getStartToken().generate()));
		beaver.Parser parser = Configuration.newParser();
		try {
			MainWindow.rootNode = (NodeInterface) parser.parse(scanner);
		} catch ( IOException | Exception e) {
			throw new ParserExeption(e.getMessage(),e);
		}
	}

}
