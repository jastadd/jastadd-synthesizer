package org.jastadd.automaticasteditor;

import org.jastadd.prettyprint.interfaces.NodeInterface;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;

public class RightSide extends VBox {

	TextArea textArea;
	NodeInterface node;

	public RightSide(final TextArea textArea, final NodeInterface node) {
		super();
		this.textArea = textArea;
		
		this.setMinWidth(400);
		this.setMaxWidth(400);
		this.setPrefWidth(400);
		this.getChildren().clear();
		
		changeNode(node);
		MainWindow.treeChanged.addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(final ObservableValue<? extends Boolean> observable,final Boolean oldValue,final Boolean newValue) {
				if(!node.getStartToken().isStart()){
					textArea.selectRange(node.getStartToken().getPrev().getEndIndexAtToken(),
						node.getEndToken().getEndIndexAtToken());
				}
			}
		});
	}

	public void changeNode(NodeInterface node) {
		this.node = node;
		Node gui = (Node) node.getPart();
		if(gui == null){
			gui = new NodeEditArea(node, this);
		}
		getChildren().clear();
		getChildren().add(gui);
		
		if (!node.getStartToken().isStart()) {
			textArea.selectRange(node.getStartToken().getPrev().getEndIndexAtToken(),
					node.getEndToken().getEndIndexAtToken());
		}

	}

}
