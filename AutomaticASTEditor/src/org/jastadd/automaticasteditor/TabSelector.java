package org.jastadd.automaticasteditor;

import javafx.geometry.Side;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableSelectionModel;

public class TabSelector extends TabPane {
	
	public TabSelector() {
		final StructureEditTab structureEdit = new StructureEditTab();
		final FreeEditTab freeEdit = new FreeEditTab();
		getTabs().add(structureEdit);
		getTabs().add(freeEdit);
		setSide(Side.BOTTOM);
		setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		final SingleSelectionModel<Tab> mode = getSelectionModel();
		setSelectionModel(new SingleSelectionModel<Tab>() {
			private int last;

			@Override
			protected Tab getModelItem(int index) {
				mode.select(index);
				return mode.getSelectedItem();
			}

			@Override
			protected int getItemCount() {
				return 2;
			}
			
			@Override
			public void select(int index) {
				if(index == 0 && last == 1){
					if(!freeEdit.canSwitch()){
						super.select(1);
						this.last = 1;
						return;
					}else{
						structureEdit.hasBeenSelected();
					}
				}
				else if(index == 1 && last == 0){
					freeEdit.hasBeenSelected();
				}
				this.last = index;
				super.select(index);
			}
		});
	}
}
