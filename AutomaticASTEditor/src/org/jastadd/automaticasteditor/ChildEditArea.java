package org.jastadd.automaticasteditor;

import java.lang.reflect.Method;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.interfaces.NodeInterface;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;

public class ChildEditArea extends BorderPane {

	public ChildEditArea(NodeInterface node, String name) {

		ChoiceBox<Class<?>> cb1 = new ChoiceBox<>();
		ObservableList<Class<?>> cbItems = cb1.getItems();
		try {
			Method m = node.getClass().getMethod("get" + name);
			Class<?>[] classes = MainWindow.subClassExtractor.getSubClasses(m.getReturnType());
			cbItems.addAll(classes);
			cb1.getSelectionModel().select(0);
		} catch (NoSuchMethodException | SecurityException | IllegalArgumentException e) {
			throw new RuntimeException("Error in getting List", e);
		}
		setLeft(new Label(name));
		setCenter(cb1);
		handleChild(node, name, cb1);
	}

	private void handleChild(final NodeInterface node,final String name,final ChoiceBox<Class<?>> cb1) {
		cb1.addEventHandler(ActionEvent.ANY, new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				Class<?> itm = cb1.getSelectionModel().getSelectedItem();
				Editors.editChild(node, name, itm, MainWindow.subClassExtractor);
				MainWindow.treeChanged.setChanged();
			}
		});
	}
}
