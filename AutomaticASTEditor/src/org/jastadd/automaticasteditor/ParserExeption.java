package org.jastadd.automaticasteditor;

public class ParserExeption extends Exception {

	public ParserExeption() {
		super();
	}

	public ParserExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ParserExeption(String message, Throwable cause) {
		super(message, cause);
	}

	public ParserExeption(String message) {
		super(message);
	}

	public ParserExeption(Throwable cause) {
		super(cause);
	}
	
	

}
