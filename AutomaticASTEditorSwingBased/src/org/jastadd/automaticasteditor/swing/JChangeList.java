package org.jastadd.automaticasteditor.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.interfaces.NodeInterface;

public class JChangeList extends JMenu {

	private CompilerContatiner cc;
	private NodeInterface ni;
	private String name;

	public JChangeList(CompilerContatiner cc, NodeInterface ni) {
		super("Change");
		this.cc = cc;
		this.ni = ni;
		this.name = Editors.getListNameFor(ni);
		
		Class<?>[] classes = Editors.getClassesList(ni.getParent().getParent(), name, cc.getSubclassExtractor());
		for (Class<?> cl : classes) {
			add(new SelectListMenuItem(cl));
		}
	}
	
	private class SelectListMenuItem extends JMenuItem implements ActionListener {
		Class<?> cl;

		public SelectListMenuItem(Class<?> cl) {
			super(cl.getSimpleName());
			this.cl = cl;
			addActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			Editors.changeElement(ni, cl, cc.getSubclassExtractor());
			cc.hasChangeTree();
		}

	}
}
