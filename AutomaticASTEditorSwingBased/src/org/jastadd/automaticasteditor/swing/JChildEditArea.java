package org.jastadd.automaticasteditor.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.deriver.SubclassExtractor;
import org.jastadd.prettyprint.interfaces.NodeInterface;

public class JChildEditArea extends JPanel implements ActionListener {

	private NodeInterface node;
	private String name;
	private CompilerContatiner cc;
	private Class<?>[] classes;

	public JChildEditArea(NodeInterface node, String name,CompilerContatiner cc) {
		setLayout(new BorderLayout());
		this.node = node;
		this.name = name;
		this.cc = cc;
		classes = Editors.getClasses(node, name, cc.getSubclassExtractor());
		String[] names  = new String[classes.length];
		for (int i = 0; i < classes.length; i++) {
			names[i] = classes[i].getSimpleName();
		}
		add(new JLabel(name), BorderLayout.NORTH);
		
		JComboBox<String> combo = new JComboBox<>();
		combo = new JComboBox<>(names);
		
		combo.setSelectedItem(Editors.getCurrentClass(node, name).getSimpleName());
		
		combo.addActionListener(this);
		if(classes.length > 1){
			add(combo);
		}else {
			add(new JLabel(classes[0].getSimpleName()));
		}
		setMaximumSize(new Dimension(30000,(int) getPreferredSize().getHeight()));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JComboBox<Class<?>> combo = (JComboBox<Class<?>>) e.getSource();
		Class<?> SelectedClass = classes[combo.getSelectedIndex()];
		Editors.editChild(node, name, SelectedClass, cc.getSubclassExtractor());
		cc.hasChangeTree();
	}

}
