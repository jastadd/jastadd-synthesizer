package org.jastadd.automaticasteditor.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.interfaces.NodeListInterface;
import org.jastadd.prettyprint.interfaces.NodeOptInterface;

public class JRemoveOpt extends JMenuItem implements ActionListener{

	private CompilerContatiner cc;
	private NodeInterface ni;
	private NodeOptInterface<NodeInterface> opt;

	public JRemoveOpt(CompilerContatiner cc, NodeInterface ni, NodeOptInterface<NodeInterface> opt) {
		super("Remove");
		this.cc = cc;
		this.ni = ni;
		this.opt = opt;
		addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		opt.removeChild(0);
		cc.hasChangeTree();
	}

}
