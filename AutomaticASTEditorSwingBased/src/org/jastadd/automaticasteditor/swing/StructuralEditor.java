package org.jastadd.automaticasteditor.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;

import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.tokenlist.CommentToken;
import org.jastadd.prettyprint.tokenlist.SignificantToken;
import org.jastadd.prettyprint.tokenlist.Token;

public class StructuralEditor extends JPanel implements Observer {
	CompilerContatiner cc;
	private OuterBox ob;
	private SidePanel sidePanel;
	private JScrollPane scroll;
	volatile private Point vp = null;

	public StructuralEditor(CompilerContatiner cc) {
		super();
		cc.addObserver(this);
		setLayout(new BorderLayout());
		this.cc = cc;
		render();
	}

	private void render() {
		removeAll();
		sidePanel = new SidePanel(cc);
		Token tokens = cc.getRoot().getStartToken();
		Iterator<Token> itr = Token.getIterator(tokens.getStartToken(), tokens.getEndToken());
		ob = new OuterBox();
		InnerBox ib = new InnerBox();
		while (itr.hasNext()) {
			Token t = itr.next();
			Object tmp = t.getASTNode().getInlinePart(cc);
			if (tmp == null) {
				ib = addCorrectItem(ib, t);
			} else {
				ib.add((Component) tmp);
				if (t != t.getASTNode().getEndToken()) {
					while (itr.hasNext() && itr.next() != t.getASTNode().getEndToken())
						;
				}
			}
		}
		ob.add(ib);
		scroll = new JScrollPane(ob);
		if(vp != null){
			scroll.getViewport().setViewPosition(vp);
		}
		add(scroll, BorderLayout.CENTER);
		add(new JScrollPane(sidePanel), BorderLayout.EAST);
		revalidate();
	}

	private InnerBox addCorrectItem(InnerBox ib, Token t) {
		if (t.getValue().equals("\n")) {
			ob.add(ib);
			ib = new InnerBox();
		} else if (t.getValue().equals(" ")) {
			ib.add(Box.createRigidArea(new Dimension(5, 0)));
		} else if (t.getValue().equals("\t")) {
			ib.add(Box.createRigidArea(new Dimension(20, 0)));
		} else if (t instanceof SignificantToken && ((SignificantToken) t).hasName()){
			ib.add(new InlineName(t, cc));
		} else if (t instanceof CommentToken){
			ib.add(new EditComment((CommentToken) t , cc));
		}
		else {
			ib.add(new PartLable(t, t.getValue(), sidePanel,cc));
		}
		return ib;
	}

	@Override
	public void update(Observable o, Object arg) {
		NodeInterface node = sidePanel.getNode();
		vp = scroll.getViewport().getViewPosition();
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				render();
				sidePanel.setASTNode(node);
			}
		});
	}
}
