package org.jastadd.automaticasteditor.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.interfaces.NodeListInterface;

public class JAddList extends JMenu {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	CompilerContatiner cc;
	NodeInterface ni;
	private String listName;

	public JAddList(CompilerContatiner cc, NodeInterface ni, String child) {
		super("Add:" + child);
		this.cc = cc;
		this.ni = ni;
		this.listName = child;
		

		Class<?>[] classes = Editors.getClassesList(ni, listName, cc.getSubclassExtractor());
		for (Class<?> cl : classes) {
			add(new SelectListMenuItem(cl));
		}

	}

	private class SelectListMenuItem extends JMenuItem implements ActionListener {
		Class<?> cl;

		public SelectListMenuItem(Class<?> cl) {
			super(cl.getSimpleName());
			this.cl = cl;
			addActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			NodeListInterface<NodeInterface> list = Editors.extractItems(ni, listName);
			Editors.addToList(list, cl, cc.getSubclassExtractor());
			cc.hasChangeTree();
		}

	}
}
