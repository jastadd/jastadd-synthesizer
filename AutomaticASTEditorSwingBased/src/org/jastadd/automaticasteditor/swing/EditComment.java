package org.jastadd.automaticasteditor.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.io.StringReader;

import javax.swing.JTextField;

import org.jastadd.prettyprint.exception.SyntaxError;
import org.jastadd.prettyprint.tokenlist.CommentToken;
import org.jastadd.prettyprint.tokenlist.SignificantToken;

import beaver.Scanner;

public class EditComment extends JTextField implements ActionListener,KeyListener {

	private CommentToken token;
	private CompilerContatiner cc;

	public EditComment(CommentToken t, CompilerContatiner cc) {
		super(t.getValue());
		setMaximumSize(new Dimension(getPreferredSize().width + 10, getPreferredSize().height));
		setForeground(Color.BLUE);
		addActionListener(this);
		this.token = t;
		this.cc = cc;
		addKeyListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String line = getText();
		Scanner es = cc.getConfiguration().newScanner(new StringReader(line));
		try {
			SignificantToken eoken = (SignificantToken) es.nextToken().value;
			System.out.println(eoken.generate());
			
			if (eoken.getPrev() instanceof CommentToken && es.nextToken().getId() == 0) {
				token.setValue(getText());
				setBackground(Color.BLACK);
				cc.hasChangeTree();
				
			} else {
				setBackground(Color.RED);
				setMaximumSize(getPreferredSize());
			}
		} catch (IOException | beaver.Scanner.Exception e1) {
			throw new RuntimeException("Io problem: " + "Change token", e1);
		} catch (SyntaxError e2) {
			setBackground(Color.RED);
		}

	}

	@Override
	public void keyTyped(KeyEvent e) {
		setMaximumSize(new Dimension(getPreferredSize().width + 10, getPreferredSize().height));
		getParent().revalidate();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
	}

}
