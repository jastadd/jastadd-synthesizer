package org.jastadd.automaticasteditor.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Method;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.interfaces.NodeInterface;

import beaver.Scanner;

public class JTextEditArea extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private NodeInterface node;
	private String name;
	private CompilerContatiner cc;
	private JTextField field;

	public JTextEditArea(NodeInterface node, String name, CompilerContatiner cc) {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		
		this.node = node;
		this.name = name;
		this.cc = cc;
		field = new JTextField(Editors.getValue(node, name));
		add(new JLabel("<" + name + ">"));
		add(field);
		JButton btn = new JButton("change");
		add(btn);
		btn.addActionListener(this);
		setMaximumSize(new Dimension(30000,(int) getPreferredSize().getHeight()));
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String line = field.getText();
		Scanner es = cc.getConfiguration().newScanner(new StringReader(line));
		try {
			boolean match = node.ownMatcher(name) && node.match(name, line);
			if(!match){
				match = (es.nextToken().getId() == ((NodeInterface) node).getToken(name).getToken()
					&& es.nextToken().getId() == 0);
			}
			if (match) {
				Editors.setValue(name, line, node);
				field.setBackground(Color.WHITE);
				cc.hasChangeTree();
			} else {
				field.setBackground(Color.RED);
			}
		} catch (IOException | beaver.Scanner.Exception e1) {
			throw new RuntimeException("Io problem: " + "set" + name, e1);
		}
		
	}

}