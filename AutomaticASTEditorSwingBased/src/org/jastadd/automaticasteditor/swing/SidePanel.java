package org.jastadd.automaticasteditor.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jastadd.prettyprint.interfaces.NodeInterface;


public class SidePanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private NodeInterface node;
	private CompilerContatiner cc;



	public SidePanel(CompilerContatiner cc) {
		super();
		setPreferredSize(new Dimension(300, 100));;
		this.cc = cc;
	}
	
	
	
	
	public void setASTNode(NodeInterface node){
		if(node == null){
			return;
		}
		removeAll();
		this.node = node;
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		if(node.getPart(cc) == null){
			createDefaultLayout(node);
		}else{
			add((Component) node.getPart(cc));
		}
		repaint();
		revalidate();
		
	}




	private void createDefaultLayout(NodeInterface node) {
		String[] tokens = this.node.getTokens();
		String[] ops = this.node.getOps();
		String[] lists = this.node.getLists();
		String[] children = this.node.getChildren();

		int count = 0;
		
		add(new JLabel(node.getClass().getSimpleName() + "::="),0,count);
		count ++;

		count = tokens.length + 1;
		for (int i = 0; i < children.length; i++) {
			add(new JChildEditArea((NodeInterface) node, children[i],cc));
		}

		count += children.length + 1;
		for (int i = 0; i < ops.length; i++) {
			add(new JOpsEditArea((NodeInterface) node, ops[i],cc));
		}

		count += ops.length + 1;
		for (int i = 0; i < lists.length; i++) {
			add(new JListEditArea((NodeInterface) node, lists[i],cc));
		}
		for (int i = 0; i < tokens.length; i++) {
			add(new JTextEditArea(node, tokens[i], cc));
		}
		count += lists.length;
		
		add(new JPartenList(node, this));
		
		System.out.println("New AST");
		
		add(Box.createVerticalGlue());
	}
	
	public NodeInterface getNode() {
		return node;
	}
}