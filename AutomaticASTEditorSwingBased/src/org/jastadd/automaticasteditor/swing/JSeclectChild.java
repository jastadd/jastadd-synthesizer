package org.jastadd.automaticasteditor.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.interfaces.NodeInterface;

public class JSeclectChild extends JMenu {
	CompilerContatiner cc;
	NodeInterface ni;
	private String childName;

	public JSeclectChild(CompilerContatiner cc, NodeInterface ni, String child) {
		super(child);
		this.cc = cc;
		this.ni = ni;
		this.childName = child;

		Class<?>[] classes = Editors.getClasses(ni, childName, cc.getSubclassExtractor());
		for (Class<?> cl : classes) {
			add(new SelectChildMenuItem(cl));
		}

	}

	private class SelectChildMenuItem extends JMenuItem implements ActionListener {
		Class<?> cl;

		public SelectChildMenuItem(Class<?> cl) {
			super(cl.getSimpleName());
			this.cl = cl;
			addActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			Editors.editChild(ni, childName, cl, cc.getSubclassExtractor());
			cc.hasChangeTree();
		}

	}

}
