package org.jastadd.automaticasteditor.swing;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.StringReader;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import javax.swing.text.StringContent;

import org.jastadd.prettyprint.exception.SyntaxError;
import org.jastadd.prettyprint.interfaces.NodeInterface;

import beaver.Parser;
import beaver.Parser.Exception;
import beaver.Scanner;

public class FreeEditor extends JPanel implements ActionListener, Observer {

	private JTextArea textArea;
	private CompilerContatiner cc;
	private JLabel errorLable;

	public FreeEditor(CompilerContatiner cc) {
		setLayout(new BorderLayout());
		PlainDocument pd = new PlainDocument();
		try {
			pd.insertString(0, cc.getRoot().getStartToken().generate(), null);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		pd.putProperty(PlainDocument.tabSizeAttribute, 2);
		textArea = new JTextArea(pd);

		add(new JScrollPane(textArea), BorderLayout.CENTER);
		JButton check = new JButton("check");
		check.addActionListener(this);
		add(check, BorderLayout.SOUTH);
		errorLable = new JLabel();
		add(errorLable, BorderLayout.NORTH);
		this.cc = cc;
		cc.addObserver(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Scanner scanner = cc.getConfiguration().newScanner(new StringReader(textArea.getText()));
		Parser parser = cc.getConfiguration().newParser();
		NodeInterface node = null;
		try {
			node = (NodeInterface) parser.parse(scanner);
		} catch (IOException | Exception | SyntaxError e1) {
			errorLable.setText(e1.getMessage());
			return;
		}
		cc.setRoot(node);
		cc.makePretty();
		cc.hasChangeTree();
	}

	@Override
	public void update(Observable o, Object arg) {
		textArea.setText(cc.getRoot().getStartToken().generate());
	}

}
