package org.jastadd.automaticasteditor.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import org.jastadd.prettyprint.formatter.NewLineToken;
import org.jastadd.prettyprint.tokenlist.CommentToken;
import org.jastadd.prettyprint.tokenlist.Token;
import org.jastadd.prettyprint.tokenlist.WhiteSpaceToken;

public class JAddComment extends JMenuItem implements ActionListener {

	private Token token;
	private CompilerContatiner cc;

	public JAddComment(Token token, CompilerContatiner cc) {
		
		super("Add comment");
		this.token = token;
		this.cc = cc;
		addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		token.insertAfter(new WhiteSpaceToken("\n"));
		token.insertAfter(new CommentToken("// fill me"));
		cc.hasChangeTree();

	}

}
