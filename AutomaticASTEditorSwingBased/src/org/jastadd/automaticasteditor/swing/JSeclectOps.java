package org.jastadd.automaticasteditor.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.interfaces.NodeInterface;

public class JSeclectOps extends JMenu {
	CompilerContatiner cc;
	NodeInterface ni;
	private String opsName;

	public JSeclectOps(CompilerContatiner cc, NodeInterface ni, String child) {
		super(child);
		this.cc = cc;
		this.ni = ni;
		this.opsName = child;
		
		add(new SelectNoneMenuItem());

		Class<?>[] classes = Editors.getClasses(ni, opsName, cc.getSubclassExtractor());
		for (Class<?> cl : classes) {
			add(new SelectOpsMenuItem(cl));
		}

	}

	private class SelectOpsMenuItem extends JMenuItem implements ActionListener {
		Class<?> cl;

		public SelectOpsMenuItem(Class<?> cl) {
			super(cl.getSimpleName());
			this.cl = cl;
			addActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			Editors.addOpt(ni, opsName, cl, cc.getSubclassExtractor());
			cc.hasChangeTree();
		}

	}
	
	private class SelectNoneMenuItem extends JMenuItem implements ActionListener {

		public SelectNoneMenuItem() {
			super("None");
			addActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			Editors.removeOpt(ni, opsName);
			cc.hasChangeTree();
		}

	}

}
