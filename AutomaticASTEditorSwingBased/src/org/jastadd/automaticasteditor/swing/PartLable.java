package org.jastadd.automaticasteditor.swing;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import org.jastadd.prettyprint.tokenlist.CommentToken;
import org.jastadd.prettyprint.tokenlist.Token;

public class PartLable extends JLabel implements MouseListener{


	/**
	 * 
	 */
	private static final long serialVersionUID = 8439686959358293647L;
	private Token token;
	private SidePanel sidePanel;
	private CompilerContatiner cc;
	
	public PartLable(Token t,String text,SidePanel sidePanel,CompilerContatiner cc) {
		super(text);
		addMouseListener(this);
		Map<TextAttribute, Object> attr = new HashMap<>();
		attr.put(TextAttribute.FOREGROUND, getTextColor(t));
		setFont(getFont().deriveFont(attr));
		token = t;
		this.sidePanel = sidePanel; 
		this.cc = cc;
	}
	
	
	
	
	
	private Color getTextColor(Token t){
		if (t instanceof CommentToken){
			return Color.BLUE;
		}
		return Color.BLACK;
	}
	

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
		if(SwingUtilities.isLeftMouseButton(e)){
			sidePanel.setASTNode(token.getASTNode());
			sidePanel.repaint();
			sidePanel.revalidate();
		}else if(SwingUtilities.isRightMouseButton(e)){
			new ASTNodeEditMenu(token, cc, e);
			
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		Map<TextAttribute, Object> attr = new HashMap<>();
		attr.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
		setFont(getFont().deriveFont(attr));
	}

	@Override
	public void mouseExited(MouseEvent e) {
		Map<TextAttribute, Object> attr = new HashMap<>();
		attr.put(TextAttribute.UNDERLINE, null);
		setFont(getFont().deriveFont(attr));
	}
}
