package org.jastadd.automaticasteditor.swing;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.jastadd.prettyprint.interfaces.NodeInterface;

public class JPartenList extends JList<NodeInterface> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2725965904667732455L;
	private NodeInterface node;
	private SidePanel sidePanel;
	
	public JPartenList(NodeInterface node, SidePanel sidePanel) {
		this.node = node;
		this.sidePanel = sidePanel;
		setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		setLayoutOrientation(JList.VERTICAL);
		setListData(node.getPartents().toArray(new NodeInterface[0]));
		DefaultListSelectionModel dlsm = new DefaultListSelectionModel();
		setSelectionModel(dlsm);
		setMaximumSize(new Dimension(30000,(int) getPreferredSize().getHeight()));
		dlsm.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				NodeInterface selsectedParent = node.getPartents().get(e.getFirstIndex());
				SwingUtilities.invokeLater(new Runnable() {
					
					@Override
					public void run() {
						sidePanel.setASTNode(selsectedParent);
						sidePanel.repaint();
						sidePanel.revalidate();
						
					}
				});
			}
		});
		
	}

}
