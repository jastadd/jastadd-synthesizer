package org.jastadd.automaticasteditor.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.interfaces.NodeListInterface;

public class JRemoveList extends JMenuItem implements ActionListener{

	private CompilerContatiner cc;
	private NodeInterface ni;
	private NodeListInterface<NodeInterface> list;

	public JRemoveList(CompilerContatiner cc, NodeInterface ni, NodeListInterface<NodeInterface> list) {
		super("Remove");
		this.cc = cc;
		this.ni = ni;
		this.list = list;
		addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int res = 0;
		for(int i = 0; i < list.getNumChild(); i++){
			if(list.getChild(i) == ni){
				res = i;
			}
		}
		Editors.removeChild(list, res);
		cc.hasChangeTree();
	}

}
