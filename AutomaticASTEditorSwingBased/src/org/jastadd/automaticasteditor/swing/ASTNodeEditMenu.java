package org.jastadd.automaticasteditor.swing;

import java.awt.event.MouseEvent;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.interfaces.NodeListInterface;
import org.jastadd.prettyprint.interfaces.NodeOptInterface;
import org.jastadd.prettyprint.tokenlist.Token;

public class ASTNodeEditMenu extends JPopupMenu {

	NodeInterface ni;
	CompilerContatiner cc;
	MouseEvent me;

	public ASTNodeEditMenu(Token token , CompilerContatiner cc, MouseEvent me) {
		super("Edit ni");
		this.ni = token.getASTNode();
		this.cc = cc;
		this.me = me;
		add(new JMenuItem("Edit: " + ni.getClass().getSimpleName()));

		String[] tokens = this.ni.getTokens();
		String[] ops = this.ni.getOps();
		String[] lists = this.ni.getLists();
		String[] children = this.ni.getChildren();

		for (int i = 0; i < children.length; i++) {
			Class<?>[] classes = Editors.getClasses(ni, children[i], cc.getSubclassExtractor());
			if (classes.length > 1) {
				add(new JSeclectChild(cc, ni, children[i]));
			}
		}

		for (int i = 0; i < ops.length; i++) {
			add(new JSeclectOps(cc, ni, ops[i]));
		}

		for (int i = 0; i < lists.length; i++) {
			add(new JAddList(cc, (NodeInterface) ni, lists[i]));
		}

		if (ni.getParent() instanceof NodeListInterface<?>) {
			NodeListInterface<NodeInterface> list = (NodeListInterface<NodeInterface>) ni.getParent();
			add(new JRemoveList(cc, ni, list));
			JChangeList jcl = new JChangeList(cc,ni);
			if(jcl.getItemCount() > 1){
				add(jcl);
			}
		} else if (ni.getParent() instanceof NodeOptInterface<?>) {
			NodeOptInterface<NodeInterface> list = (NodeOptInterface<NodeInterface>) ni.getParent();
			add(new JRemoveOpt(cc, ni, list));
			//add(new JChangeOpt(cc,ni,list)); // TO DO
		} else {
			JChangeChild jcl = new JChangeChild(cc,ni);
			if(jcl.getItemCount() > 1){
				add(jcl);
			}
		}
		
		add(new JAddComment(token,cc));

		show(me.getComponent(), me.getX(), me.getY());
	}

}
