package org.jastadd.automaticasteditor.swing;


import java.awt.Component;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jastadd.prettyprint.interfaces.Configuration;

import beaver.Parser;
import beaver.Scanner;



public class Main {
	
	private JFrame window;

//	public Main() {
//		window = new JFrame("hello");
//		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		JPanel rowPane = new JPanel();
//		rowPane.setLayout(new BoxLayout(rowPane, BoxLayout.Y_AXIS));
//		
//		
//		JPanel pane = new JPanel();
//		pane.setLayout(new BoxLayout(pane, BoxLayout.X_AXIS));
//		pane.add(new PartLable("hej"));
//		pane.add(Box.createRigidArea(new Dimension(5,0)));
//		pane.add(new PartLable("på"));
//		pane.add(Box.createRigidArea(new Dimension(5,0)));
//		pane.add(new PartLable("dig"));
//		pane.add(Box.createRigidArea(new Dimension(5,0)));
//		pane.add(new PartLable("din"));
//		JPanel pane2 = new JPanel();
//		pane2.setLayout(new BoxLayout(pane2, BoxLayout.X_AXIS));
//		pane2.add(new PartLable("hej"));
//		pane2.add(new PartLable("på"));
//		pane.setAlignmentX(Component.LEFT_ALIGNMENT);
//		rowPane.add(pane);
//		
//		pane2.setAlignmentX(Component.LEFT_ALIGNMENT);
//		rowPane.add(pane2);
//		window.getContentPane().add(rowPane);
//		window.pack();
//		window.setVisible(true);
//		
//	}

	public Main(CompilerContatiner cc) {
		window = new JFrame("hello");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//StructuralEditor strucEditor = new StructuralEditor(cc);
		SwingASTEditor editor = new SwingASTEditor(cc);
		
		window.getContentPane().add(editor);
		window.pack();
		window.setVisible(true);
	}
	

}
