package org.jastadd.automaticasteditor.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.StringReader;

import javax.swing.BorderFactory;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;

import org.jastadd.automaticasteditor.swing.CompilerContatiner;
import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.exception.SyntaxError;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.tokenlist.SignificantToken;
import org.jastadd.prettyprint.tokenlist.Token;

import beaver.Scanner;

public class InlineName extends JTextField implements ActionListener, KeyListener, MouseListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	CompilerContatiner cc;
	private SignificantToken token;

	public InlineName(Token token, CompilerContatiner cc) {
		super(token.getValue());

		this.token = (SignificantToken) token;
		this.cc = cc;
		setMaximumSize(new Dimension(getPreferredSize().width + 10, getPreferredSize().height));
		addActionListener(this);
		addKeyListener(this);
		addMouseListener(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		NodeInterface node = token.getASTNode();
		String name = token.getName();

		String line = getText();
		Scanner es = cc.getConfiguration().newScanner(new StringReader(line));
		try {
			boolean match = node.ownMatcher(name) && node.match(name, line);
			if (!match) {
				match = (es.nextToken().getId() == ((NodeInterface) node).getToken(name).getToken()
						&& es.nextToken().getId() == 0);
			}
			if (match) {
				Editors.setValue(name, line, node);
				setBackground(Color.GRAY);
				cc.hasChangeTree();
			} else {
				setBackground(Color.RED);
			}
		} catch (IOException | beaver.Scanner.Exception e1) {
			throw new RuntimeException("Io problem: " + "set" + name, e1);
		} catch (SyntaxError e2) {
			setBackground(Color.RED);
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		setMaximumSize(new Dimension(getPreferredSize().width + 10, getPreferredSize().height));
		getParent().revalidate();
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void mouseClicked(MouseEvent e) {

	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (SwingUtilities.isRightMouseButton(e)) {
			new ASTNodeEditMenu(token, cc, e);

		}

	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {

	}

}
