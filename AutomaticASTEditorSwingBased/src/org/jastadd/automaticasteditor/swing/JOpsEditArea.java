package org.jastadd.automaticasteditor.swing;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.interfaces.NodeInterface;

public class JOpsEditArea extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private NodeInterface node;
	private String name;
	private CompilerContatiner cc;
	private JComboBox<Class<?>> combo;
	private JCheckBox checkBox;
	private boolean checked;

	public JOpsEditArea(NodeInterface node, String name, CompilerContatiner cc) {
		this.node = node;
		this.name = name;
		this.cc = cc;
		Class<?>[] classes = Editors.getClasses(node, name, cc.getSubclassExtractor());
		checked = Editors.hasOpt(node, name);
		checkBox = new JCheckBox("has" + name, checked);
		checkBox.addActionListener(this);
		combo = new JComboBox<>(classes);
		add(new JLabel("[" + name + "]"));
		if (checked) {
			combo.setSelectedItem(Editors.getCurrentClass(node, name));
		}
		if (classes.length > 1) {
			add(combo);
			combo.addActionListener(this);
		} else {
			add(new JLabel(classes[0].getSimpleName()));
		}
		add(checkBox);
		setMaximumSize(new Dimension(30000, (int) getPreferredSize().getHeight()));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (checkBox.isSelected() && !checked) {
			Class<?> SelectedClass = (Class<?>) combo.getSelectedItem();
			Editors.addOpt(node, name, SelectedClass, cc.getSubclassExtractor());
		} else if (!checkBox.isSelected() && checked) {
			Editors.removeOpt(node, name);
		} else if (checkBox.isSelected() && checked) {
			Editors.removeOpt(node, name);
			Class<?> SelectedClass = (Class<?>) combo.getSelectedItem();
			Editors.addOpt(node, name, SelectedClass, cc.getSubclassExtractor());
		}
		cc.hasChangeTree();
	}

}