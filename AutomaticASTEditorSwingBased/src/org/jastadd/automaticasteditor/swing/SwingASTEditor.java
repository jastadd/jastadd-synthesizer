package org.jastadd.automaticasteditor.swing;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SwingASTEditor extends JTabbedPane {
	
	public SwingASTEditor(CompilerContatiner cc) {
		super(BOTTOM);
		addTab("Stuct",new StructuralEditor(cc));
		addTab("Free", new FreeEditor(cc));
	}


}
