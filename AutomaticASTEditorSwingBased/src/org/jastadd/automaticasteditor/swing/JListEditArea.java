package org.jastadd.automaticasteditor.swing;

import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.interfaces.NodeListInterface;

public class JListEditArea extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private NodeInterface node;
	private String name;
	private CompilerContatiner cc;
	private JComboBox combo;
	private JCheckBox checkBox;
	private JList<NodeInterface> jList;
	private JButton addButton;
	private JButton removeButton;
	private NodeListInterface<NodeInterface> itms;
	private Class<?>[] classes;
	
	public JListEditArea(NodeInterface node, String name, CompilerContatiner cc) {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.node = node;
		this.name = name;
		this.cc = cc;
		itms = Editors.extractItems(node, name);
		NodeInterface[] data = itratorToArray(itms);
		jList = new JList<>(data);
		jList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		jList.setLayoutOrientation(JList.VERTICAL);
		add(new JLabel(name + "*"));
		add(jList);
		
		classes = Editors.getClassesList(node, name, cc.getSubclassExtractor());
		String[] names  = new String[classes.length];
		for (int i = 0; i < classes.length; i++) {
			names[i] = classes[i].getSimpleName();
		}
		combo = new JComboBox<>(names);
		if(classes.length > 1){
			add(combo);
		}else{
			add(new JLabel(classes[0].getSimpleName()));
		}
		addButton = new JButton("add");
		removeButton = new JButton("remove");
		addButton.addActionListener(this);
		removeButton.addActionListener(this);
		add(addButton);
		add(removeButton);
		setMaximumSize(new Dimension(30000,(int) getPreferredSize().getHeight()));
	}

	private NodeInterface[] itratorToArray(NodeListInterface<NodeInterface> itms) {
		ArrayList<NodeInterface> list = new ArrayList<>();
		for(NodeInterface n : itms){
			list.add(n);
		}
		NodeInterface[] data = list.toArray(new NodeInterface[0]);
		return data;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == addButton) {
			Class<?> SelectedClass = classes[combo.getSelectedIndex()];
			Editors.addToList(itms , SelectedClass, cc.getSubclassExtractor());
		} else if (e.getSource() == removeButton) {
			int i = jList.getSelectedIndex();
			if(i >= 0){
				Editors.removeChild(itms, i);
			}
		}
		cc.hasChangeTree();
	}
}