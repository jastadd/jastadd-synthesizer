package org.jastadd.automaticasteditor.swing;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.Observable;

import org.jastadd.prettyprint.deriver.SubclassExtractor;
import org.jastadd.prettyprint.formatter.PrettyMaker;
import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.interfaces.NodeInterface;

import beaver.Parser;
import beaver.Scanner;

public class CompilerContatiner extends Observable {
	private Configuration conf;
	private Scanner scan;
	private SubclassExtractor se;
	private NodeInterface root;
	private Parser parser;
	
	
	public CompilerContatiner(Configuration conf, String sourcePath, String classPath) throws Exception {
		super();
		this.conf = conf;
		this.scan = conf.newScanner(new FileReader(sourcePath));
		this.se = new SubclassExtractor(new File(classPath),conf);
		this.parser = conf.newParser();
		this.root = (NodeInterface) parser.parse(scan);
		PrettyMaker.makePretty(root,conf);
	}
	public CompilerContatiner(Configuration conf, Reader source, String classPath) throws Exception {
		super();
		this.conf = conf;
		this.scan = conf.newScanner(source);
		this.se = new SubclassExtractor(new File(classPath),conf);
		this.parser = conf.newParser();
		this.root = (NodeInterface) parser.parse(scan);
		PrettyMaker.makePretty(root,conf);
	}
	
	public SubclassExtractor getSubclassExtractor(){
		return se;
	}
	
	public Configuration getConfiguration(){
		return conf;
	}
	
	public NodeInterface getRoot(){
		return root;
	}
	public void setRoot(NodeInterface root){
		this.root = root;
	}
	
	public void makePretty(){
		PrettyMaker.makePretty(root,conf);
	}
	
	public void hasChangeTree(){
		root.flushTreeCache();
		makePretty();
		setChanged();
		notifyObservers();
	}
	
}
