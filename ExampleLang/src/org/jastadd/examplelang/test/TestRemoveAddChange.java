package org.jastadd.examplelang.test;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.RandomAccessFile;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.deriver.SubclassExtractor;
import org.jastadd.prettyprint.formatter.PrettyMaker;
import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.tokenlist.Token;
import org.junit.Before;
import org.junit.Test;

import gen.Bar;
import gen.Example;
import gen.ExampleScanner;
import gen.Indent;
import gen.Line;
import gen.List;
import gen.OnExample;
import gen.Option;
import gen.Pindent;
import gen.Test2;

import gen.ExampleScanner;
import gen.ExampleParser;
public class TestRemoveAddChange extends TestHelpers {


	private Configuration conf;

	@Before
	public void initialize() {
		this.conf =  new Configuration(ExampleScanner.class, ExampleParser.class);
	}
	
	@Test
	public void test() throws Exception {
		Indent a = (Indent) testSyntax("removeTest1.example");
		Token start = Token.getStart(a.getStartToken());
		PrettyMaker.makePretty(a,conf);
		byte[] bytes = readFile("testscript/removeTest1.example");
		assertEquals(new String(bytes), start.generate());
		a.getStmtList().getChild(0).removeTokens();
		a.getStmtList().removeChild(0);
		PrettyMaker.makePretty(a,conf);
		PrettyMaker.makePretty(a,conf);
		bytes = readFile("testscript/removeTest1.exampleres");
		assertEquals(new String(bytes), start.generate());

	}

	@Test
	public void testType0() throws Exception {
		Indent a = (Indent) testSyntax("removeTest0.example");
		Token start = Token.getStart(a.getStartToken());
		byte[] bytes = readFile("testscript/removeTest0.example");
		PrettyMaker.makePretty(a,conf);
		assertEquals(new String(bytes), start.generate());
		List n = (List) a.getStmtList().getChild(0).getChild(0);
		n.getChild(1).removeTokens();
		n.removeChild(1);
		PrettyMaker.makePretty(a,conf);
		PrettyMaker.makePretty(a,conf);
		bytes = readFile("testscript/removeTest0.exampleres");
		assertEquals(new String(bytes), start.generate());

	}

	@Test
	public void testType2() throws Exception {
		Indent a = (Indent) testSyntax("removeTest2.example");
		Token start = Token.getStart(a.getStartToken());
		byte[] bytes = readFile("testscript/removeTest2.example");
		PrettyMaker.makePretty(a,conf);
		assertEquals(new String(bytes), start.generate());
		List n = (List) a.getStmtList().getChild(0).getChild(0).getChild(0).getChild(0);
		Editors.removeChild(n, 1);
		PrettyMaker.makePretty(a,conf);
		PrettyMaker.makePretty(a,conf);
		bytes = readFile("testscript/removeTest2.exampleres");
		assertEquals(new String(bytes), start.generate());
	}

	@Test
	public void testType3() throws Exception {
		Indent a = (Indent) testSyntax("removeTest3.example");
		Token start = Token.getStart(a.getStartToken());
		byte[] bytes = readFile("testscript/removeTest3.example");
		PrettyMaker.makePretty(a,conf);
		assertEquals(new String(bytes), start.generate());
		List n = (List) a.getStmtList().getChild(0).getChild(0).getChild(0).getChild(0).getChild(0).getChild(0);
		Editors.removeChild(n, 1);
		PrettyMaker.makePretty(a,conf);
		PrettyMaker.makePretty(a,conf);
		bytes = readFile("testscript/removeTest3.exampleres");
		assertEquals(new String(bytes), start.generate());
	}

	@Test
	public void testType4() throws Exception {
		Indent a = (Indent) testSyntax("removeTest4.example");
		Token start = Token.getStart(a.getStartToken());
		byte[] bytes = readFile("testscript/removeTest4.example");
		PrettyMaker.makePretty(a,conf);
		assertEquals(new String(bytes), start.generate());
		List n = (List) a.getStmtList();
		Option o = (Option) n.getChild(0);
		Editors.removeOpt(o, "OnExample");
		PrettyMaker.makePretty(a,conf);
		PrettyMaker.makePretty(a,conf);
		bytes = readFile("testscript/removeTest4.exampleres");
		assertEquals(new String(bytes), start.generate());
	}

	@Test
	public void testAddOpt() throws Exception {
		Indent a = (Indent) testSyntax("removeTest4.exampleres");
		Token start = Token.getStart(a.getStartToken());
		byte[] bytes = readFile("testscript/removeTest4.exampleres");
		PrettyMaker.makePretty(a,conf);
		assertEquals(new String(bytes), start.generate());
		List n = (List) a.getStmtList();
		Option o = (Option) n.getChild(0);
		Editors.addOpt(o, "OnExample", OnExample.class,
				new SubclassExtractor(new File("/Users/alfred/git/jastadd-synthesizer/ExampleLang/gen/gen/"),conf));
		PrettyMaker.makePretty(a,conf);
		PrettyMaker.makePretty(a,conf);
		bytes = readFile("testscript/removeTest4.example");
		assertEquals(new String(bytes), start.generate());
	}

	@Test
	public void testAddToList() throws Exception {
		Indent a = (Indent) testSyntax("removeTest4.example");
		Token start = Token.getStart(a.getStartToken());
		byte[] bytes = readFile("testscript/removeTest4.example");
		PrettyMaker.makePretty(a,conf);
		assertEquals(new String(bytes), start.generate());
		List n = (List) a.getStmtList();
		Editors.addToList(n, Example.class,
				new SubclassExtractor(new File("/Users/alfred/git/jastadd-synthesizer/ExampleLang/gen/gen/"),conf));
		PrettyMaker.makePretty(a,conf);
		PrettyMaker.makePretty(a,conf);
		bytes = readFile("testscript/removeTest4.exampleres2");
		assertEquals(new String(bytes), start.generate());
	}

	@Test
	public void testChange() throws Exception {
		Indent a = (Indent) testSyntax("removeTest4.example");
		Token start = Token.getStart(a.getStartToken());
		byte[] bytes = readFile("testscript/removeTest4.example");
		PrettyMaker.makePretty(a,conf);
		assertEquals(new String(bytes), start.generate());
		List n = (List) a.getStmtList();
		Editors.addToList(n, Line.class,
				new SubclassExtractor(new File("/Users/alfred/git/jastadd-synthesizer/ExampleLang/gen/gen/"),conf));
		PrettyMaker.makePretty(a,conf);
		Editors.setValue("line", "hej", n.getChild(1));
		PrettyMaker.makePretty(a,conf);
		PrettyMaker.makePretty(a,conf);
		bytes = readFile("testscript/removeTest4.exampleres3");
		assertEquals(new String(bytes), start.generate());
	}

	@Test
	public void testEditChild() throws Exception {
		Indent a = (Indent) testSyntax("removeTest4.example");
		Token start = Token.getStart(a.getStartToken());
		byte[] bytes = readFile("testscript/removeTest4.example");
		PrettyMaker.makePretty(a,conf);
		assertEquals(new String(bytes), start.generate());
		List n = (List) a.getStmtList();
		Editors.addToList(n, Test2.class,
				new SubclassExtractor(new File("/Users/alfred/git/jastadd-synthesizer/ExampleLang/gen/gen/"),conf));
		PrettyMaker.makePretty(a,conf);
		System.out.println(a.getStartToken().generate());
		System.out.println("hej");
		Editors.editChild(n.getChild(1), "Stmt", Bar.class,
				new SubclassExtractor(new File("/Users/alfred/git/jastadd-synthesizer/ExampleLang/gen/gen/"),conf));
		PrettyMaker.makePretty(a,conf);
		PrettyMaker.makePretty(a,conf);
		bytes = readFile("testscript/removeTest4.exampleres4");
		assertEquals(new String(bytes), start.generate());
	}

	@Test
	public void testAddRemoveOpt() throws Exception {
		Indent a = (Indent) testSyntax("removeTest4.exampleres");
		Token start = Token.getStart(a.getStartToken());
		byte[] bytes = readFile("testscript/removeTest4.exampleres");
		PrettyMaker.makePretty(a,conf);
		assertEquals(new String(bytes), start.generate());
		List n = (List) a.getStmtList();
		Option o = (Option) n.getChild(0);
		Editors.addOpt(o, "OnExample", OnExample.class,
				new SubclassExtractor(new File("/Users/alfred/git/jastadd-synthesizer/ExampleLang/gen/gen/"),conf));
		PrettyMaker.makePretty(a,conf);
		Editors.removeOpt(o, "OnExample");
		PrettyMaker.makePretty(a,conf);
		bytes = readFile("testscript/removeTest4.exampleres");
		assertEquals(new String(bytes), start.generate());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testPindent() throws Exception {
		Indent a = (Indent) testSyntax("removeTest4.exampleres");
		Token start = Token.getStart(a.getStartToken());
		byte[] bytes = readFile("testscript/removeTest4.exampleres");
		PrettyMaker.makePretty(a,conf);
		assertEquals(new String(bytes), start.generate());
		List n = (List) a.getStmtList();
		Editors.removeChild(n, 0);
		PrettyMaker.makePretty(a,conf);
		Editors.addToList(n, Pindent.class,
				new SubclassExtractor(new File("/Users/alfred/git/jastadd-synthesizer/ExampleLang/gen/gen/"),conf));
		PrettyMaker.makePretty(a,conf);
		Editors.addToList(n, Pindent.class,
				new SubclassExtractor(new File("/Users/alfred/git/jastadd-synthesizer/ExampleLang/gen/gen/"),conf));
		PrettyMaker.makePretty(a,conf);
		Editors.removeChild(n, 0);
		PrettyMaker.makePretty(a,conf);
		bytes = readFile("testscript/removeTest4.exampleres5");
		assertEquals(new String(bytes), start.generate());
	}

	public byte[] readFile(String path) throws Exception {
		RandomAccessFile f = new RandomAccessFile(path, "r");
		byte[] data = new byte[(int) f.length()];
		f.read(data);
		return data;
	}
}
