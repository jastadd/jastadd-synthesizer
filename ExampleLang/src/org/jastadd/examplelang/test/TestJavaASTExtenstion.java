package org.jastadd.examplelang.test;

import static org.junit.Assert.*;

import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.tokenlist.SignificantToken;
import org.junit.Before;
import org.junit.Test;
import gen.ASTNode;
import gen.Bar;
import gen.Ex3;
import gen.Example;
import gen.ExampleParser;
import gen.ExampleScanner;
import gen.Indent;
import gen.Line;
import gen.Option;
import gen.Tx;

public class TestJavaASTExtenstion {
	

	private Configuration conf;

	@Before
	public void initialize() {
		this.conf =  new Configuration(ExampleScanner.class, ExampleParser.class);
	}
	@Test
	public void test() {
		ASTNode l = new Line("hej");
		String[] tokens = l.getTokens();
		assertEquals("line", tokens[0]);

		SignificantToken st = new SignificantToken("hello", (short) 1);
		l.addToken("line", st);
		assertEquals(st, l.getToken("line"));

		l = new Example();
		tokens = l.getTokens();
		assertEquals(0, tokens.length);

		l = new gen.Test("", "");
		tokens = l.getTokens();
		assertEquals("t1", tokens[0]);
		assertEquals("t2", tokens[1]);
		
		l = new Ex3(new Tx(), "1", "2", "3");
		tokens = l.getTokens();
		assertEquals("n1", tokens[0]);
		assertEquals("n2", tokens[1]);
		assertEquals("n3", tokens[2]);
	}

	@Test
	public void testOps() {
		ASTNode l = new Line("hej");
		String[] ops = l.getOps();
		assertEquals(0, ops.length);

		l = new Option();
		ops = l.getOps();
		assertEquals(2, ops.length);
		assertEquals("OnLine", ops[0]);
		assertEquals("OnExample", ops[1]);
	}

	@Test
	public void testList() throws NoSuchMethodException, SecurityException {
		ASTNode l = new Line("hej");
		String[] lists = l.getLists();
		assertEquals(0, lists.length);

		l = new Indent();
		lists = l.getLists();
		assertEquals(1, lists.length);
		assertEquals("Stmt", lists[0]);
	}

	@Test
	public void testChildren() {
		ASTNode l = new Line("hej");
		String[] children = l.getChildren();
		assertEquals(0, children.length);

		l = new Bar();
		children = l.getChildren();
		assertEquals(2, children.length);
		assertEquals("Example", children[0]);
		assertEquals("Indent", children[1]);
	}
}
