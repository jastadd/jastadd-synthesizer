package org.jastadd.examplelang.test;

import static org.junit.Assert.*;

import org.jastadd.prettyprint.tokenlist.CommentToken;
import org.jastadd.prettyprint.tokenlist.Token;
import org.junit.Test;

public class TestTokenListElement {


	@Test
	public void test() {
		Token head = Token.createListStart();
		assertEquals(head, head.getNext().getPrev());
	}

	@Test
	public void testInstertAfterOne() {
		Token head = Token.createListStart();
		Token end = head.getNext();
		Token elem = new CommentToken(" ");
		head.insertAfter(elem);
		assertEquals(elem, head.getNext());
		assertEquals(elem, end.getPrev());
	}

	@Test
	public void testInsertAfterThree() {
		Token head = Token.createListStart();
		Token end = head.getNext();
		Token elem1 = new CommentToken("1");
		Token elem2 = new CommentToken("2");
		Token elem3 = new CommentToken("3");
		head.insertAfter(elem3);
		head.insertAfter(elem2);
		head.insertAfter(elem1);

		assertEquals(elem1, head.getNext());
		assertEquals(elem2, elem1.getNext());
		assertEquals(elem3, elem2.getNext());
		assertEquals(end, elem3.getNext());

		assertEquals(head, elem1.getPrev());
		assertEquals(elem1, elem2.getPrev());
		assertEquals(elem2, elem3.getPrev());
		assertEquals(elem3, end.getPrev());
	}

	@Test
	public void testInsertAfterThreeAndRemove() {
		Token head = Token.createListStart();
		Token end = head.getNext();
		Token elem1 = new CommentToken("1");
		CommentToken elem2 = new CommentToken("2");
		Token elem3 = new CommentToken("3");
		head.insertAfter(elem3);
		head.insertAfter(elem2);
		head.insertAfter(elem1);

		assertEquals(elem1, head.getNext());
		assertEquals(elem2, elem1.getNext());
		assertEquals(elem3, elem2.getNext());
		assertEquals(end, elem3.getNext());

		assertEquals(head, elem1.getPrev());
		assertEquals(elem1, elem2.getPrev());
		assertEquals(elem2, elem3.getPrev());
		assertEquals(elem3, end.getPrev());

		elem2.doRemove();

		assertEquals(elem1, head.getNext());
		assertEquals(elem3, elem1.getNext());
		assertEquals(end, elem3.getNext());

		assertEquals(head, elem1.getPrev());
		assertEquals(elem1, elem3.getPrev());
		assertEquals(elem3, end.getPrev());
	}

	@Test
	public void testInsertBeforeThree() {
		Token head = Token.createListStart();
		Token end = head.getNext();
		Token elem1 = new CommentToken("1");
		Token elem2 = new CommentToken("2");
		Token elem3 = new CommentToken("3");

		end.insertBefore(elem1);
		end.insertBefore(elem2);
		end.insertBefore(elem3);

		assertEquals(elem1, head.getNext());
		assertEquals(elem2, elem1.getNext());
		assertEquals(elem3, elem2.getNext());
		assertEquals(end, elem3.getNext());

		assertEquals(head, elem1.getPrev());
		assertEquals(elem1, elem2.getPrev());
		assertEquals(elem2, elem3.getPrev());
		assertEquals(elem3, end.getPrev());
		assertEquals(head, Token.getStart(elem2));
		assertEquals(head, Token.getStart(head));
		assertEquals(head, Token.getStart(end));
		assertEquals(end, Token.getEnd(elem2));
		assertEquals(end, Token.getEnd(end));
		assertEquals(end, Token.getEnd(head));
	}

	@Test
	public void testValue() {
		Token elem = new CommentToken("hello");
		assertEquals("hello", elem.getValue());
		assertTrue(elem.hasValue());
		assertFalse(elem.getPrev().hasValue());
		assertFalse(elem.getNext().hasValue());
	}

	@Test
	public void testIsBefore() {
		Token head = Token.createListStart();
		Token end = head.getNext();
		Token elem1 = new CommentToken("1");
		Token elem2 = new CommentToken("2");
		Token elem3 = new CommentToken("3");
		head.insertAfter(elem3);
		head.insertAfter(elem2);
		head.insertAfter(elem1);
		assertTrue(elem1.isBefore(elem2));
		assertTrue(!elem2.isBefore(elem1));
		assertTrue(elem2.isBefore(elem2));
		assertTrue(elem1.isBefore(elem3));
		assertTrue(!elem3.isBefore(elem1));

	}

	@Test
	public void testIsAfter() {
		Token head = Token.createListStart();
		Token end = head.getNext();
		Token elem1 = new CommentToken("1");
		Token elem2 = new CommentToken("2");
		Token elem3 = new CommentToken("3");
		head.insertAfter(elem3);
		head.insertAfter(elem2);
		head.insertAfter(elem1);
		assertTrue(!elem1.isAfter(elem2));
		assertTrue(elem2.isAfter(elem1));
		assertTrue(elem2.isAfter(elem2));
		assertTrue(!elem1.isAfter(elem3));
		assertTrue(elem3.isAfter(elem1));

	}
}
