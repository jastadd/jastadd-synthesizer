package org.jastadd.examplelang.test;

import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;

import org.jastadd.prettyprint.exception.SyntaxError;
import org.jastadd.prettyprint.interfaces.Configuration;
import org.junit.Before;

import gen.ExampleParser;
import gen.ExampleScanner;


public class TestHelpers {
	
	private Configuration conf;

	@Before
	public void initialize() {
		this.conf =  new Configuration(ExampleScanner.class, ExampleParser.class);
	}

	protected static Object parse(File file) throws IOException, Exception {
		ExampleScanner scanner = new ExampleScanner(new FileReader(file));
		ExampleParser parser = new ExampleParser();
		return parser.parse(scanner);
	}

	protected Object testSyntax(String filename) {
		try {
			return parse(new File("testscript", filename));
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unexpected error while parsing '" + filename + "': " + e.getMessage());
		}
		return null;
	}

	public TestHelpers() {
		super();
	}

	public void testSyntaxError(String filename) {
		try {
			// Beaver reports syntax error on the standard error.
			// We want to discard these messages since we expect syntax error.
			System.setErr(new PrintStream(new ByteArrayOutputStream()));
	
			parse(new File("testscript", filename));
	
			fail("syntax is valid, expected syntax error");
		} catch (beaver.Parser.Exception  e) {
			// ok (syntax error)!
		
		} catch (SyntaxError e) {
			// ok (syntax error)!
		}
		catch (Exception e) {
			fail("IO error while trying to parse '" + filename + "': "
				+ e.getMessage());
		}
	}
	
	protected void testSyntaxString(String pp) {
		ExampleScanner scanner = new ExampleScanner(new StringReader(pp));
		ExampleParser parser = new ExampleParser();
		try {
			parser.parse(scanner);
		} catch (Exception e) {
			e.printStackTrace();
			fail("Unexpected error while parsing : " + e.getMessage());
		}
		
	}

}