package org.jastadd.examplelang.test;

import static org.junit.Assert.*;

import java.io.File;

import org.jastadd.prettyprint.deriver.Editors;
import org.jastadd.prettyprint.deriver.SubclassExtractor;
import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.junit.Before;
import org.junit.Test;

import gen.Ba;
import gen.Baa;
import gen.Bar;
import gen.Example;
import gen.ExampleParser;
import gen.ExampleScanner;
import gen.Indent;
import gen.Line;
import gen.List;
import gen.OnExample;
import gen.Opt;
import gen.Pindent;

public class TestGetInfo {

	private Configuration conf;
	private SubclassExtractor sce;

	@Before
	public void initialize() {
		this.conf =  new Configuration(ExampleScanner.class, ExampleParser.class);
		this.sce = new SubclassExtractor(new File("/Users/alfred/git/jastadd-synthesizer/ExampleLang/gen/gen/"),conf);
	}
	@Test
	public void testNotHasOpt() {
		NodeInterface baa = new Baa(new Opt<>(), new List<>());
		assertFalse(Editors.hasOpt(baa,"OnExample"));
	
	}
	
	@Test
	public void testHasOpt() {
		NodeInterface baa = new Baa(new Opt<>(new OnExample(new Example())), new List<>());
		assertTrue(Editors.hasOpt(baa,"OnExample"));
	}
	
	@Test
	public void testClassList() {
		NodeInterface baa = new Baa(new Opt<>(), new List<>());
		assertEquals(Editors.getClasses(baa,"OnExample",sce)[0].getSimpleName(),"OnExample");
	
	}
	
	@Test
	public void testgetList() {
		Ba baba = new Ba();
		NodeInterface baa = new Baa(new Opt<>(), new List<>(baba));
		for(NodeInterface n : Editors.extractItems(baa,"Ba")){
			assertEquals(baba, n);
		}
	}
	
	@Test
	public void testSubclassList() {
		Ba baba = new Ba();
		NodeInterface baa = new Baa(new Opt<>(), new List<>(baba));
		assertEquals("Ba", Editors.getClassesList(baa, "Ba", sce)[0].getSimpleName());
	}
	
	@Test
	public void testGetValuse() {
		NodeInterface baa = new Line("hej") ;
		assertEquals("hej", Editors.getValue(baa, "line"));
	}
	
	@Test
	public void testGetCurrentClass() {
		NodeInterface baa = new Bar(new Example(), new Pindent()) ;
		assertEquals(Example.class,Editors.getCurrentClass(baa, "Example"));
		assertEquals(Pindent.class,Editors.getCurrentClass(baa, "Indent"));
	}
	
	@Test
	public void testListName() {
		Ba baba = new Ba();
		NodeInterface baa = new Baa(new Opt<>(), new List<>(baba));
		assertEquals("Ba",Editors.getListNameFor(baba));
	}

	@Test
	public void testGetIndex() {
		Ba baba = new Ba();
		Baa baa = new Baa(new Opt<>(), new List<>(baba));
		assertEquals(0,Editors.getListIndex(baba));
	}
	
	@Test
	public void testCange() {
		Ba baba = new Ba();
		Baa baa = new Baa(new Opt<>(), new List<>(baba));
		Editors.changeElement(baba,Ba.class,sce);
		assertNotEquals(baba,baa.getBa(0));
	}
	@Test
	public void testgetName() {
		Indent ind = new Indent();
		Bar bar = new Bar(new Example(),ind);
		assertEquals("Indent",Editors.getNameFor(ind));
	}
	
	@Test
	public void testExtractItem() {
		Indent ind = new Indent();
		Bar bar = new Bar(new Example(),ind);
		assertEquals(ind,Editors.extractItem(bar, "Indent"));
	}
}
