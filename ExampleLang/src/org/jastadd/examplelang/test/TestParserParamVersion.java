package org.jastadd.examplelang.test;


import java.util.Arrays;
import java.util.Collection;

import org.jastadd.prettyprint.interfaces.Configuration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import gen.ExampleScanner;

@RunWith(Parameterized.class)
public class TestParserParamVersion extends TestHelpers {
	

	  @Parameters
	    public static Collection<Object[]> data() {
	        return Arrays.asList(new Object[][] {
	                 {"example1.example"},
	                 {"example2.example"},
	                 {"example3.example"},
	                 {"example4.example"},
	                 {"example5.example"},
	                 {"example6.example"},
	                 {"example7.example"},
	           });
	    }
	
	@Parameter
	public String file;

	@Test
	public void test() {
		testSyntax(file);
	}

}
