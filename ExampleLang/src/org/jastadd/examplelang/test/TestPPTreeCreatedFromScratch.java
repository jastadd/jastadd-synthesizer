package org.jastadd.examplelang.test;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.jastadd.prettyprint.formatter.PrettyMaker;
import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.tokenlist.CommentToken;
import org.jastadd.prettyprint.tokenlist.Token;
import org.junit.Before;
import org.junit.Test;

import gen.Example;
import gen.ExampleScanner;
import gen.Indent;
import gen.Line;
import gen.List;
import gen.Stmt;
import gen.ExampleParser;
import gen.ExampleScanner;

public class TestPPTreeCreatedFromScratch {
	

	private Configuration conf;

	@Before
	public void initialize() {
		this.conf =  new Configuration(ExampleScanner.class, ExampleParser.class);
	}
	@Test
	public void test() {
		Indent a = new Indent(new List<Stmt>(new Example()));
		PrettyMaker.makePretty(a,conf);
		Iterator<Token> itr = Token.getIterator(Token.getStart(a.getStartToken()), Token.getEnd(a.getEndToken()));
		String s = "";
		while (itr.hasNext()) {
			Token elem = itr.next();
			s += elem.getValue();
		}
		a.getStmt(0).getEndToken().insertAfter(new CommentToken("\\\\hello"));
		assertEquals("indent{\n\texample\n}", s);
		a.getStmts().insertChild(new Example(), 0);
		a.getStmts().insertChild(new Line("hello"), 1);
		
		PrettyMaker.makePretty(a,conf);
		itr = Token.getIterator(Token.getStart(a.getStartToken()), Token.getEnd(a.getEndToken()));
		s = "";
		while (itr.hasNext()) {
			Token elem = itr.next();
			s += elem.getValue();
		}
		assertEquals("indent{\n\texample\n\thello\n\texample\\\\hello\n}", s);
	}

}
