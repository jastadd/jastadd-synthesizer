package org.jastadd.examplelang.test;



import static org.junit.Assert.*;

import org.jastadd.prettyprint.tokenlist.CommentToken;
import org.jastadd.prettyprint.tokenlist.Token;
import org.junit.Test;

import gen.Example;
import gen.Indent;
import gen.List;
import gen.Stmt;
import gen.Test2;


public class testSetEndRecursive {
	
	@Test
	public void testTop() throws Exception {
		Token start = Token.createListStart();
		
		CommentToken p = new CommentToken("p");
		CommentToken t = new CommentToken("t");
		CommentToken o = new CommentToken("o");
		start.insertAfter(p);
		start.insertAfter(t);
		start.insertAfter(o);
		
		Example ex1 = new Example();
		Example ex2 = new Example();
		
		List<Stmt> list = new List<Stmt>(ex1,ex2);
		Indent in = new Indent(list);
		Test2 test = new Test2(in);
		
		test.setEndToken(t);
		in.setEndToken(o);
		list.setEndToken(t);
		ex1.setEndToken(t);
		ex2.setEndToken(o);
		
		test.setEndTokenRecursive(p);
		
		assertEquals(p,test.getEndToken());
		assertEquals(o,in.getEndToken());
		assertEquals(t,list.getEndToken());
		assertEquals(t,ex1.getEndToken());
		assertEquals(o,ex2.getEndToken());
	}
	
	@Test
	public void testButtom() throws Exception {
		Token start = Token.createListStart();
		
		CommentToken p = new CommentToken("p");
		CommentToken t = new CommentToken("t");
		CommentToken o = new CommentToken("o");
		start.insertAfter(p);
		start.insertAfter(o);
		start.insertAfter(t);
		
		Example ex1 = new Example();
		Example ex2 = new Example();
		
		List<Stmt> list = new List<Stmt>(ex1,ex2);
		Indent in = new Indent(list);
		Test2 test = new Test2(in);
		
		test.setEndToken(t);
		in.setEndToken(o);
		list.setEndToken(t);
		ex1.setEndToken(t);
		ex2.setEndToken(t);
		
		assertEquals(t,test.getEndToken());
		assertEquals(o,in.getEndToken());
		assertEquals(t,list.getEndToken());
		assertEquals(t,ex1.getEndToken());
		assertEquals(t,ex2.getEndToken());
		
		ex1.setEndTokenRecursive(p);
		
		assertEquals(p,test.getEndToken());
		assertEquals(o,in.getEndToken());
		assertEquals(p,list.getEndToken());
		assertEquals(p,ex1.getEndToken());
		assertEquals(t,ex2.getEndToken());
	}
	
	@Test
	public void testButtom2() throws Exception {
		Token start = Token.createListStart();
		
		CommentToken p = new CommentToken("p");
		CommentToken t = new CommentToken("t");
		CommentToken o = new CommentToken("o");
		start.insertAfter(p);
		start.insertAfter(t);
		start.insertAfter(o);
		
		Example ex1 = new Example();
		Example ex2 = new Example();
		
		List<Stmt> list = new List<Stmt>(ex1,ex2);
		Indent in = new Indent(list);
		Test2 test = new Test2(in);
		
		test.setEndToken(t);
		in.setEndToken(o);
		list.setEndToken(t);
		ex1.setEndToken(t);
		ex2.setEndToken(o);
		
		ex1.setEndTokenRecursive(p);
		
		assertEquals(p,test.getEndToken());
		assertEquals(o,in.getEndToken());
		assertEquals(p,list.getEndToken());
		assertEquals(p,ex1.getEndToken());
		assertEquals(o,ex2.getEndToken());
	}
}
