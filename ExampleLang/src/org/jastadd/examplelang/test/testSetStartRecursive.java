package org.jastadd.examplelang.test;



import static org.junit.Assert.*;

import org.jastadd.prettyprint.tokenlist.CommentToken;
import org.jastadd.prettyprint.tokenlist.Token;
import org.junit.Test;

import gen.Example;
import gen.Indent;
import gen.List;
import gen.Stmt;
import gen.Test2;


public class testSetStartRecursive {
	
	@Test
	public void testTop() throws Exception {
		Token start = Token.createListStart();
		
		CommentToken p = new CommentToken("p");
		CommentToken t = new CommentToken("t");
		CommentToken o = new CommentToken("o");
		start.insertAfter(p);
		start.insertAfter(t);
		start.insertAfter(o);
		
		Example ex1 = new Example();
		Example ex2 = new Example();
		
		List<Stmt> list = new List<Stmt>(ex1,ex2);
		Indent in = new Indent(list);
		Test2 test = new Test2(in);
		
		test.setStartToken(t);
		in.setStartToken(o);
		list.setStartToken(t);
		ex1.setStartToken(t);
		ex2.setStartToken(o);
		
		test.setStartTokenRecursive(p);
		
		assertEquals(p,test.getStartToken());
		assertEquals(o,in.getStartToken());
		assertEquals(t,list.getStartToken());
		assertEquals(t,ex1.getStartToken());
		assertEquals(o,ex2.getStartToken());
	}
	
	@Test
	public void testButtom() throws Exception {
		Token start = Token.createListStart();
		
		CommentToken p = new CommentToken("p");
		CommentToken t = new CommentToken("t");
		CommentToken o = new CommentToken("o");
		start.insertAfter(p);
		start.insertAfter(o);
		start.insertAfter(t);
		
		Example ex1 = new Example();
		Example ex2 = new Example();
		
		List<Stmt> list = new List<Stmt>(ex1,ex2);
		Indent in = new Indent(list);
		Test2 test = new Test2(in);
		
		test.setStartToken(t);
		in.setStartToken(o);
		list.setStartToken(t);
		ex1.setStartToken(t);
		ex2.setStartToken(o);
		
		assertEquals(t,test.getStartToken());
		assertEquals(o,in.getStartToken());
		assertEquals(t,list.getStartToken());
		assertEquals(t,ex1.getStartToken());
		assertEquals(o,ex2.getStartToken());
		
		ex1.setStartTokenRecursive(p);
		
		assertEquals(p,test.getStartToken());
		assertEquals(o,in.getStartToken());
		assertEquals(p,list.getStartToken());
		assertEquals(p,ex1.getStartToken());
		assertEquals(o,ex2.getStartToken());
	}
	
	@Test
	public void testButtom2() throws Exception {
		Token start = Token.createListStart();
		
		CommentToken p = new CommentToken("p");
		CommentToken t = new CommentToken("t");
		CommentToken o = new CommentToken("o");
		start.insertAfter(p);
		start.insertAfter(t);
		start.insertAfter(o);
		
		Example ex1 = new Example();
		Example ex2 = new Example();
		
		List<Stmt> list = new List<Stmt>(ex1,ex2);
		Indent in = new Indent(list);
		Test2 test = new Test2(in);
		
		test.setStartToken(t);
		in.setStartToken(o);
		list.setStartToken(t);
		ex1.setStartToken(t);
		ex2.setStartToken(o);
		
		ex1.setStartTokenRecursive(p);
		
		assertEquals(p,test.getStartToken());
		assertEquals(o,in.getStartToken());
		assertEquals(p,list.getStartToken());
		assertEquals(p,ex1.getStartToken());
		assertEquals(o,ex2.getStartToken());
	}
}
