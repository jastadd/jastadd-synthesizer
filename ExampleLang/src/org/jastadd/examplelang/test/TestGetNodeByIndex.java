package org.jastadd.examplelang.test;

import static org.junit.Assert.*;


import org.jastadd.prettyprint.formatter.PrettyMaker;
import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.tokenlist.Token;
import org.junit.Before;
import org.junit.Test;

import gen.ASTNode;
import gen.ExampleParser;
import gen.ExampleScanner;

public class TestGetNodeByIndex extends TestHelpers{
	
	private Configuration conf;

	@Before
	public void initialize() {
		this.conf =  new Configuration(ExampleScanner.class, ExampleParser.class);
	}

	@Test
	public void test() {
		ASTNode a = (ASTNode) testSyntax("example8.example");
		PrettyMaker.makePretty(a,conf);
		
		assertEquals(a,a.getStartToken().getNodeAtIndex(0).getASTNode());
		assertEquals(a,a.getStartToken().getNodeAtIndex(1).getASTNode());
		assertEquals(a.getChild(0).getChild(0),a.getStartToken().getNodeAtIndex(10).getASTNode());
		assertEquals(a,a.getStartToken().getNodeAtIndex(13).getASTNode());
		
		System.out.println("Token is:" + Token.getStart(a.getStartToken()).getNext() + ": this!");
		
		
		assertEquals(6, Token.getStart(a.getStartToken()).getNext().getEndIndexAtToken());
		
		assertEquals(0, Token.getStart(a.getStartToken()).getEndIndexAtToken());
		
	}

}
