package org.jastadd.examplelang.test;



import static org.junit.Assert.*;

import org.jastadd.prettyprint.tokenlist.CommentToken;
import org.jastadd.prettyprint.tokenlist.Token;
import org.junit.Test;

import gen.Example;
import gen.Indent;
import gen.List;
import gen.Stmt;
import gen.Test2;


public class testSetAll {
	
	
	@Test
	public void testButtom() throws Exception {
		Token start = Token.createListStart();
		
		CommentToken p = new CommentToken("p");
		CommentToken t = new CommentToken("t");
		CommentToken o = new CommentToken("o");
		
		start.insertAfter(p);
		start.insertAfter(t);
		start.insertAfter(o);
		
		Example ex1 = new Example();
		Example ex2 = new Example();
		
		List<Stmt> list = new List<Stmt>(ex1,ex2);
		Indent in = new Indent(list);
		Test2 test = new Test2(in);
		
		test.setEndToken(t);
		in.setEndToken(o);
		list.setEndToken(t);
		ex1.setEndToken(t);
		ex2.setEndToken(o);
		
		test.setStartToken(t);
		in.setStartToken(o);
		list.setStartToken(t);
		ex1.setStartToken(t);
		ex2.setStartToken(o);
		
		test.changeAll(t);
		
		assertEquals(o,test.getEndToken());
		assertEquals(o,in.getEndToken());
		assertEquals(o,list.getEndToken());
		assertEquals(o,ex1.getEndToken());
		assertEquals(o,ex2.getEndToken());
		
		assertEquals(p,test.getStartToken());
		assertEquals(o,in.getStartToken());
		assertEquals(p,list.getStartToken());
		assertEquals(p,ex1.getStartToken());
		assertEquals(o,ex2.getStartToken());
		
	}
	
	@Test
	public void testButtom2() throws Exception {
		Token start = Token.createListStart();
		
		CommentToken p = new CommentToken("p");
		CommentToken t = new CommentToken("t");
		CommentToken o = new CommentToken("o");
		
		start.insertAfter(p);
		start.insertAfter(t);
		start.insertAfter(o);
		
		Example ex1 = new Example();
		Example ex2 = new Example();
		
		List<Stmt> list = new List<Stmt>(ex1,ex2);
		Indent in = new Indent(list);
		Test2 test = new Test2(in);
		
		test.setEndToken(t);
		in.setEndToken(o);
		list.setEndToken(t);
		ex1.setEndToken(t);
		ex2.setEndToken(o);
		
		test.setStartToken(t);
		in.setStartToken(o);
		list.setStartToken(t);
		ex1.setStartToken(t);
		ex2.setStartToken(o);
		
		ex1.changeAll(t);
		
		assertEquals(o,test.getEndToken());
		assertEquals(o,in.getEndToken());
		assertEquals(o,list.getEndToken());
		assertEquals(o,ex1.getEndToken());
		assertEquals(o,ex2.getEndToken());
		
		assertEquals(p,test.getStartToken());
		assertEquals(o,in.getStartToken());
		assertEquals(p,list.getStartToken());
		assertEquals(p,ex1.getStartToken());
		assertEquals(o,ex2.getStartToken());
		
	}
}
