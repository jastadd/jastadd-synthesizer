package org.jastadd.examplelang.test;

import static org.junit.Assert.*;

import java.io.RandomAccessFile;

import org.jastadd.prettyprint.formatter.PrettyMaker;
import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.tokenlist.Token;
import org.junit.Before;
import org.junit.Test;

import gen.ASTNode;
import gen.ExampleScanner;
import gen.Line;
import gen.ExampleParser;
import gen.ExampleScanner;

public class TestPlaceComment extends TestHelpers {
	
	private Configuration conf;

	@Before
	public void initialize() {
		this.conf =  new Configuration(ExampleScanner.class, ExampleParser.class);
	}

	@Test
	public void test() throws Exception {
		ASTNode a = (ASTNode) testSyntax("placeComment0.example");
		Token start = Token.getStart(a.getStartToken());
		PrettyMaker.makePretty(a,conf);
		byte[] bytes = readFile("testscript/placeComment0.example");
		assertEquals(new String(bytes), start.generate());	

		a.getStartToken().generate();
		Line l = (Line) a.getChild(0).getChild(0);
		l.getToken("line").setValue("hej1");
		l.setline("hej1");
		PrettyMaker.makePretty(a,conf);
		bytes = readFile("testscript/placeComment0.exampleres");
		assertEquals(new String(bytes), start.generate());	

	}
	public byte[] readFile(String path) throws Exception {
		RandomAccessFile f = new RandomAccessFile(path, "r");
		byte[] data = new byte[(int) f.length()];
		f.read(data);
		return data;
	}
}
