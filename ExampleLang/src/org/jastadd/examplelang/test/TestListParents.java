package org.jastadd.examplelang.test;

import static org.junit.Assert.assertEquals;

import java.io.RandomAccessFile;
import java.util.ArrayList;

import org.jastadd.prettyprint.interfaces.NodeInterface;
import org.jastadd.prettyprint.tokenlist.Token;
import org.junit.Test;

import gen.Indent;
import gen.List;
import gen.Option;

public class TestListParents extends TestHelpers {

	@Test
	public void test() throws Exception {
		Indent a = (Indent) testSyntax("removeTest1.example");
		java.util.List<NodeInterface> ni = new ArrayList<>();
		ni.add(a);
		assertEquals(ni,a.getStmtList().getChild(0).getPartents());
	}

	@Test
	public void testType0() throws Exception {
		Indent a = (Indent) testSyntax("removeTest0.example");
		List n = (List) a.getStmtList().getChild(0).getChild(0);
		java.util.List<NodeInterface> ni = new ArrayList<>();
		ni.add(a.getStmtList().getChild(0));
		ni.add(a);
		assertEquals(ni, n.getChild(1).getPartents());
	}

	@Test
	public void testType2() throws Exception {
		Indent a = (Indent) testSyntax("removeTest2.example");
		Token start = Token.getStart(a.getStartToken());
		byte[] bytes = readFile("testscript/removeTest2.example");
		assertEquals(new String(bytes), start.generate());
		List n = (List) a.getStmtList().getChild(0).getChild(0).getChild(0).getChild(0);
		java.util.List<NodeInterface> ni = new ArrayList<>();
		ni.add(a.getStmtList().getChild(0).getChild(0).getChild(0));
		ni.add(a.getStmtList().getChild(0));
		ni.add(a);
		assertEquals(ni, n.getChild(0).getPartents());
	}

	@Test
	public void testType3() throws Exception {
		Indent a = (Indent) testSyntax("removeTest3.example");
		Token start = Token.getStart(a.getStartToken());
		byte[] bytes = readFile("testscript/removeTest3.example");
		assertEquals(new String(bytes), start.generate());
		List n = (List) a.getStmtList().getChild(0).getChild(0).getChild(0).getChild(0).getChild(0).getChild(0);
		java.util.List<NodeInterface> ni = new ArrayList<>();
		ni.add(a.getStmtList().getChild(0).getChild(0).getChild(0).getChild(0).getChild(0));
		ni.add(a.getStmtList().getChild(0).getChild(0).getChild(0));
		ni.add(a.getStmtList().getChild(0));
		ni.add(a);
		assertEquals(ni, n.getChild(0).getPartents());
	}

	@Test
	public void testType4() throws Exception {
		Indent a = (Indent) testSyntax("removeTest4.example");
		Token start = Token.getStart(a.getStartToken());
		byte[] bytes = readFile("testscript/removeTest4.example");
		assertEquals(new String(bytes), start.generate());
		List n = (List) a.getStmtList();
		Option o = (Option) n.getChild(0);
		java.util.List<NodeInterface> ni = new ArrayList<>();
		ni.add(a.getStmtList().getChild(0));
		ni.add(a);
		assertEquals(ni, o.getChild(0).getPartents());
	}

	public byte[] readFile(String path) throws Exception {
		RandomAccessFile f = new RandomAccessFile(path, "r");
		byte[] data = new byte[(int) f.length()];
		f.read(data);
		return data;
	}
}
