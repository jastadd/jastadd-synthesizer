package org.jastadd.examplelang.test;

import static org.junit.Assert.*;

import java.io.File;

import org.jastadd.prettyprint.deriver.SubclassExtractor;
import org.jastadd.prettyprint.interfaces.Configuration;
import org.junit.Before;
import org.junit.Test;

import gen.Ba;
import gen.Bar;
import gen.D1;
import gen.D2;
import gen.D3;
import gen.D4;
import gen.D5;
import gen.Ex1;
import gen.Ex3;
import gen.Example;
import gen.ExampleScanner;
import gen.Indent;
import gen.Inf;
import gen.Line;
import gen.Stmt;
import gen.Test2;
import gen.Tx;


import gen.ExampleParser;

public class TestSubclassExtractor {
	
	
	
	private Configuration conf;

	@Before
	public void initialize() {
		this.conf =  new Configuration(ExampleScanner.class, ExampleParser.class);
	}

	@Test
	public void test() {
		SubclassExtractor se = new SubclassExtractor(new File("/Users/alfred/git/ExampelLang/ExampleLang/gen/gen"),conf);
		Class<?>[] classes = se.getSubClasses(Stmt.class);
		assertEquals(9, classes.length);
		for (Class<?> c : classes) {
			System.out.println(c.getName());
		}
		classes = se.getSubClasses(Ba.class);
		assertEquals(1, classes.length);
		assertEquals("gen.Ba", classes[0].getName());
		classes = se.getSubClasses(String.class);
		assertEquals(0, classes.length);
	}

	@Test
	public void testInfreAll() {
		SubclassExtractor se = new SubclassExtractor(new File("/Users/alfred/git/ExampelLang/ExampleLang/gen/gen"),conf);
		Bar bar = new Bar();
		assertEquals(null, bar.getExample());
		assertEquals(null, bar.getIndent());
		se.inferAll(bar);
		assertEquals(Example.class, bar.getExample().getClass());
		assertEquals(Indent.class, bar.getIndent().getClass());

		Test2 t = new Test2();
		se.inferAll(t);
		assertEquals(Example.class, t.getStmt().getClass());

		Ex1 e1 = new Ex1();
		se.inferAll(e1);
		assertEquals(Ex3.class, e1.getEx().getClass());
		assertEquals(Tx.class, e1.getEx().getChild(0).getClass());

		Ex3 e3 = new Ex3();
		se.inferAll(e3);
		assertEquals(Tx.class, e3.getTx().getClass());

		assertEquals("fillIn", e3.getn1());
		assertEquals("fillIn", e3.getn2());
		assertEquals("fillIn", e3.getn3());
		assertEquals("fillIn", e3.getTx().getslut());

		D1 d = new D1();
		assertEquals(null, d.getD2());
		se.inferAll(d);
		assertEquals(D2.class, d.getD2().getClass());
		assertEquals(D3.class, d.getD2().getD3().getClass());
		assertEquals(D4.class, d.getD2().getD3().getD4().getClass());
		assertEquals(D5.class, d.getD2().getD3().getD4().getD5().getClass());
		assertEquals("fillIn", d.getD2().getD3().getD4().getD5().getD6().getslut());

		boolean testInf = false;
		try {
			se.inferAll(new Inf());
		} catch (RuntimeException e) {
			testInf = true;
		}
		assertTrue(testInf);

	}
	
	@Test
	public void testInfreAllDefualtValue() {
		SubclassExtractor se = new SubclassExtractor(new File("/Users/alfred/git/ExampelLang/ExampleLang/gen/gen"),conf);
		Line line = new Line();
		assertEquals("", line.getline());
		se.inferAll(line);
		assertEquals("fillOut", line.getline());

	}

}
