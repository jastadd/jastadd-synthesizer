package org.jastadd.examplelang.test;

import static org.junit.Assert.*;

import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

import org.jastadd.prettyprint.formatter.PrettyMaker;
import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.tokenlist.Token;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import gen.ASTNode;
import gen.ExampleScanner;
import gen.ExampleParser;

@RunWith(Parameterized.class)
public class TestPrettyPrintParamVersion extends TestHelpers {
	

	private Configuration conf;

	@Before
	public void initialize() {
		this.conf =  new Configuration(ExampleScanner.class, ExampleParser.class);
	}
	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { { "format0.example" }, { "format1.example" }, { "format2.example" },
				{ "format3.example" }, { "format4.example" }, { "format5.example" },  { "format6.example" }});
	}

	@Parameter
	public String file;

	@Test
	public void test() throws Exception {
		ASTNode a = (ASTNode) testSyntax(file);
		Token start = Token.getStart(a.getStartToken());
		Token end = Token.getEnd(a.getEndToken());
		Random rand = new Random(123);
		PrettyMaker.makePretty(a,conf);
		PrettyMaker.makePretty(a,conf);
		PrettyMaker.makePretty(a,conf);
		byte[] bytes = readFile("testscript/" + file + "res");
		assertEquals(new String(bytes), start.generate());
	}

	public byte[] readFile(String path) throws Exception {
		RandomAccessFile f = new RandomAccessFile(path, "r");
		byte[] data = new byte[(int) f.length()];
		f.read(data);
		return data;
	}
}
