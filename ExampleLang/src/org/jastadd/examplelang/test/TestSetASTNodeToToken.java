package org.jastadd.examplelang.test;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.jastadd.prettyprint.formatter.PrettyMaker;
import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.tokenlist.Token;
import org.junit.Before;
import org.junit.Test;

import gen.ASTNode;
import gen.ExampleScanner;

import gen.ExampleParser;
public class TestSetASTNodeToToken extends TestHelpers {
	

	private Configuration conf;

	@Before
	public void initialize() {
		this.conf =  new Configuration(ExampleScanner.class, ExampleParser.class);
	}
	@Test
	public void test() {
		ASTNode a = (ASTNode) testSyntax("example1.example");
		a.setASTNodeToTokens();
		Iterator<Token> itr = Token.getIterator(a.getStartToken(), a.getEndToken());
		itr.hasNext();
		// while(itr.hasNext()){
		// Token to = itr.next();
		// System.out.println(to.getASTNode());
		//
		// }

		assertEquals(a, itr.next().getASTNode());
		assertEquals(a, itr.next().getASTNode());
		assertEquals(a, itr.next().getASTNode());
		assertEquals(a, itr.next().getASTNode());

		assertEquals(a.getChild(0).getChild(0).getChild(0), itr.next().getASTNode());

		assertEquals(a.getChild(0).getChild(0), itr.next().getASTNode());
		assertEquals(a.getChild(0).getChild(0), itr.next().getASTNode());
		assertEquals(a.getChild(0).getChild(0), itr.next().getASTNode());

		assertEquals(a.getChild(0).getChild(0).getChild(1), itr.next().getASTNode());
		assertEquals(a.getChild(0).getChild(0).getChild(1), itr.next().getASTNode());
		assertEquals(a.getChild(0).getChild(0).getChild(1), itr.next().getASTNode());

		assertEquals(a, itr.next().getASTNode());
		assertEquals(a, itr.next().getASTNode());
		assertEquals(a, itr.next().getASTNode());
	}

	@Test
	public void test3() {
		ASTNode a = (ASTNode) testSyntax("example1.example");
		a.setASTNodeToTokens();
		PrettyMaker.makePretty(a,conf);
		a.setASTNodeToTokens();
		Iterator<Token> itr = Token.getIterator(a.getStartToken(), a.getEndToken());
		itr.hasNext();
		// while(itr.hasNext()){
		// Token to = itr.next();
		// System.out.println(to.getASTNode());
		//
		// }

		assertEquals(a, itr.next().getASTNode());
		assertEquals(a, itr.next().getASTNode());
		assertEquals(a, itr.next().getASTNode());
		assertEquals(a, itr.next().getASTNode());

		assertEquals(a.getChild(0).getChild(0).getChild(0), itr.next().getASTNode());

		assertEquals(a.getChild(0).getChild(0), itr.next().getASTNode());
		assertEquals(a.getChild(0).getChild(0), itr.next().getASTNode());
		assertEquals(a.getChild(0).getChild(0), itr.next().getASTNode());

		assertEquals(a.getChild(0).getChild(0).getChild(1), itr.next().getASTNode());
		assertEquals(a.getChild(0).getChild(0).getChild(1), itr.next().getASTNode());
		assertEquals(a.getChild(0).getChild(0).getChild(1), itr.next().getASTNode());
		assertEquals(a.getChild(0).getChild(0).getChild(1), itr.next().getASTNode());
		assertEquals(a.getChild(0).getChild(0).getChild(1), itr.next().getASTNode());
		assertEquals(a.getChild(0).getChild(0).getChild(1), itr.next().getASTNode());
		assertEquals(a.getChild(0).getChild(0).getChild(1), itr.next().getASTNode());
		assertEquals(a.getChild(0).getChild(0).getChild(1), itr.next().getASTNode());

		assertEquals(a, itr.next().getASTNode());
		assertEquals(a, itr.next().getASTNode());
	}

	@Test
	public void test2() {
		ASTNode a = (ASTNode) testSyntax("example8.example");
		a.setASTNodeToTokens();
		PrettyMaker.makePretty(a,conf);
		a.setASTNodeToTokens();
		Iterator<Token> itr = Token.getIterator(a.getStartToken(), a.getEndToken());
		itr.hasNext();
		// while(itr.hasNext()){
		// Token to = itr.next();
		// System.out.println(to.getASTNode());
		//
		// }

		assertEquals(a, itr.next().getASTNode());
		assertEquals(a, itr.next().getASTNode());
		assertEquals(a, itr.next().getASTNode());
		assertEquals(a, itr.next().getASTNode());

		assertEquals(a.getChild(0).getChild(0), itr.next().getASTNode());

		assertEquals(a, itr.next().getASTNode());
		assertEquals(a, itr.next().getASTNode());
	}

	@Test
	public void test4() {
		ASTNode a = (ASTNode) testSyntax("example8.example");
		a.setASTNodeToTokens();
		Iterator<Token> itr = Token.getIterator(a.getStartToken(), a.getEndToken());
		itr.hasNext();
		// while(itr.hasNext()){
		// Token to = itr.next();
		// System.out.println(to.getASTNode());
		//
		// }

		assertEquals(a, itr.next().getASTNode());
		assertEquals(a, itr.next().getASTNode());
		assertEquals(a, itr.next().getASTNode());
		assertEquals(a, itr.next().getASTNode());

		assertEquals(a.getChild(0).getChild(0), itr.next().getASTNode());

		assertEquals(a, itr.next().getASTNode());
		assertEquals(a, itr.next().getASTNode());
	}

}
