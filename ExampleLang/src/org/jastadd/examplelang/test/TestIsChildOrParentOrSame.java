package org.jastadd.examplelang.test;

import static org.junit.Assert.*;


import org.jastadd.prettyprint.formatter.PrettyMaker;
import org.jastadd.prettyprint.interfaces.Configuration;
import org.junit.Before;
import org.junit.Test;

import gen.ASTNode;
import gen.Example;
import gen.ExampleParser;
import gen.ExampleScanner;

public class TestIsChildOrParentOrSame extends TestHelpers {

	private Configuration conf;

	@Before
	public void initialize() {
		this.conf =  new Configuration(ExampleScanner.class, ExampleParser.class);
	}
	
	@Test
	public void test() {
		ASTNode a = (ASTNode) testSyntax("example9.example");
		PrettyMaker.makePretty(a,conf);
		a.setASTNodeToTokens();
		
		assertTrue(a.getStartToken().getNodeAtIndex(10).getASTNode().isChildOrParentOrSame(a));
		assertTrue(a.isChildOrParentOrSame(a.getStartToken().getNodeAtIndex(10).getASTNode()));
		assertTrue(a.isChildOrParentOrSame(a));
		assertTrue(a.getStartToken().getNodeAtIndex(10).getASTNode().isChildOrParentOrSame(a.getStartToken().getNodeAtIndex(10).getASTNode()));
		assertFalse(a.isChildOrParentOrSame(new Example()));
		assertFalse(a.getStartToken().getNodeAtIndex(15).getASTNode().isChildOrParentOrSame(a.getStartToken().getNodeAtIndex(10).getASTNode()));
	}

}
