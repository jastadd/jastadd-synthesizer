package org.jastadd.examplelang.test;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import beaver.Symbol;
import gen.ASTNode;
import gen.ExampleParser;
import gen.ExampleScanner;
import gen.Indent;

import org.jastadd.prettyprint.interfaces.Configuration;
import org.jastadd.prettyprint.tokenlist.Token;

public class ParserTest extends TestHelpers {
	
	@Before
	public void initialize() {
	}

	@Test
	@Ignore
	public void testVerySimple() {
		testSyntax("verySimple.as2");
	}

	@Test
	@Ignore
	public void testSimple() {
		testSyntax("simple.as2");
	}

	@Test
	@Ignore
	public void testAssigment() {
		testSyntax("assigment.as2");
	}

	@Test
	@Ignore
	public void testComplex() {
		testSyntax("complex.as2");
	}

	@Test
	@Ignore
	public void testError() {
		testSyntaxError("error.as2");
		testSyntaxError("error2.as2");
		testSyntaxError("error3.as2");
		testSyntaxError("error4.as2");
	}

	@Test
	@Ignore
	public void testScanner() throws Exception {
		ExampleScanner scanner = new ExampleScanner(new FileReader(new File("testscript", "ver6.as2")));
		Symbol t = scanner.nextToken();
		while (t.getId() != 0) {
			System.out.println(ExampleParser.Terminals.NAMES[t.getId()] + " : " + t.value);
			t = scanner.nextToken();
		}
	}

	@Test
	public void testPP() {
		try {
			Indent i = (Indent) parse(new File("testscript", "example5.example"));
			Token.printInterval(i.getStartToken(), i.getEndToken());
			Queue<ASTNode<?>> que = new LinkedList<ASTNode<?>>();
			for (ASTNode<?> a : i.astChildren()) {
				que.offer(a);
			}
			while (!que.isEmpty()) {
				ASTNode<?> n = que.poll();
				System.out.println(n.getClass().getName());
				Token.printInterval(n.getStartToken(), n.getEndToken());
				System.out.println("------");
				for (ASTNode<?> a : n.astChildren()) {
					que.offer(a);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
