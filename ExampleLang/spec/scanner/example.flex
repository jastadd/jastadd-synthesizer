package gen; // The generated scanner will belong to the package lang.ast


import gen.ExampleParser.Terminals; // The terminals are implicitly defined in the parser
import org.jastadd.prettyprint.exception.*;
import org.jastadd.prettyprint.tokenlist.*;

%%

// define the signature for the generated scanner
%public
%final
%class ExampleScanner
%extends beaver.Scanner

// the interface between the scanner and the parser is the nextToken() method
%type beaver.Symbol 
%function nextToken 

// store line and column information in the tokens
%line
%column

// this code will be inlined in the body of the generated scanner class
%{

    private StringBuilder stringLitSb = new StringBuilder();
    private Token end = Token.createListEnd();
    
    private beaver.Symbol sym(short id) {
        Token elem = new SignificantToken(yytext(),id);
        end.insertBefore(elem);
        return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), elem);
    }
    private beaver.Symbol sym(short id,String line) {
        Token elem = new SignificantToken(line,id);
        end.insertBefore(elem);
        return new beaver.Symbol(id, yyline + 1, yycolumn + 1, line.length(), elem);
    }
    private void sym() {
        Token elem = new WhiteSpaceToken(yytext());
        end.insertBefore(elem);
    }

    private void symComment() {
        Token elem = new CommentToken(yytext());
        end.insertBefore(elem);
    }

%}
    // macros
    Line = \w+ 
    WhiteSpace = \R | \s 
    TraditionalComment   = "/*" [^*] ~"*/" | "/*" "*"+ "/"
    EndOfLineComment = "//" [^\n|\r|\r\n]*
    Comment = {TraditionalComment} | {EndOfLineComment}

%%
    <YYINITIAL>{

        //Keywords
        {WhiteSpace}  { sym(); }
        {Comment}  { symComment(); }
        "baa"    { return sym(Terminals.BAA);}
        "ba"    { return sym(Terminals.BA);}
        "na"    { return sym(Terminals.NA);}
        "batman"    { return sym(Terminals.BATMAN);}
        "example"    { return sym(Terminals.EXAMPLE);}
        "indent"    { return sym(Terminals.INDENT);}
        "pindent"    { return sym(Terminals.PINDENT);}
        "on"    { return sym(Terminals.ON);}
        "opt"    { return sym(Terminals.OPT);}
        "{"		  { return sym(Terminals.LEFT_BRACE);}
        "}"		  { return sym(Terminals.RIGHT_BRACE);}
        "\-"        { return sym(Terminals.BAR); }
        "#"        { return sym(Terminals.HASH); }
        ":"        { return sym(Terminals.END); }
        {Line}      { return sym(Terminals.LINE); }
        <<EOF>>       { return sym(Terminals.EOF); }
    }
/* error fallback */
[^]           { throw new SyntaxError("Illegal character <"+yytext()+">"); }
