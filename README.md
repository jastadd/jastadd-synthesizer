### This description is for version: [this](https://bitbucket.org/jastadd/jastadd-synthesizer/commits/50eccdc67f447365b94f5f3bb9cf60040bbe9ef7)

# JastAdd-Synthesizer #

* An embodiment of the vison to create a structural editor dynamically from form a jastadd compiler.

### How to run an example: ###
1. Clone the whole repository.
2. `cd jastadd-synthesizer/ExampleASTEditor/`
3. `ant jar`
4. `java -jar AutomaticASTEditor.jar`
5. The demo is now running.

### Project stucture ###


####The repository contains 4  Eclipse projects:####

* **PrettyPrintAST** - that is tools for a generic pretty-printer used by the editor
* **AutomaticASTEditor** - is the actual generic editor
* **ExampleLang** - is an example implementation of a language used for creating this project and testing it.
* **ExampleASTEditor** -  an example editor using ExampleLang.

#### The dependencies between the projects are the following: ####
![Bild1.jpg](https://bitbucket.org/repo/K8erzR/images/3796319066-Bild1.jpg)

#### The second part dependencies are: ####
* [JastAdd](http://jastadd.org)
* [JastAddParser](https://bitbucket.org/jastadd/jastaddparser)

#### The third pard dependencies are: ####
* [JFlex](http://www.jflex.de)
* [Beaver parser](http://beaver.sourceforge.net)

### Create your own editor ###
[See here](https://bitbucket.org/jastadd/jastadd-synthesizer/wiki/Create%20your%20own%20editor)

### Who do I talk to? ###

* alfred.akesson@cs.lth.se